main := thesis
#nonstop := -interaction=nonstopmode
nonstop :=

.PHONY: clean

all: $(main).tex $(main).bib
	pdflatex $(nonstop) -shell-escape -synctex=1 $(main).tex
	bibtex ${main}
	makeglossaries $(main)
	pdflatex $(nonstop) -shell-escape -synctex=1 $(main).tex
	makeglossaries $(main)
	#makeindex $(main)
	pdflatex $(nonstop) -shell-escape -synctex=1 $(main).tex

clean:
	@rm *.bak $(main).aux $(main).bbl $(main).blg $(main).dvi $(main).log $(main).out $(main).pdf $(main).ps $(main).glo $(main).gls $(main).lot $(main).lof $(main).toc $(main).xdy $(main).glg $(main).synctex.gz **/*.aux *.bcf *.xml *.idx *.log &> /dev/null || true
