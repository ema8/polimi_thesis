\chapter{Problem Definition and Proposed Solution} \label{chap:design}

\epigraph{
\itshape
Give yourself over to absolute pleasure. Swim the warm waters of sins of the flesh - erotic nightmares beyond any measure, and sensual daydreams to treasure forever. Can't you just see it? Don't dream it, be it.
}{
\itshape Frank-N-Furter,\\The Rocky Horror Picture Show
}$\:$\\
This chapter defines the problem we want to tackle with our work and introduces our solution, which is a workload-aware run-time resource management policy. Our proposal aims at both satisfying \gls{qos} requirement (i.e. minimum throughput) and reducing power consumption by setting a proper configuration of cores (big and LITTLE) and their frequency, and balancing tasks threads on available resources. \Cref{sec:problem_definition} presents the rationale of this thesis work. \Cref{sec:proposed_solution} outlines the details of our proposed solution.

\section{Problem Definition}\label{sec:problem_definition}
The actual scenario is characterized by various and flexible on-demand computing workloads, with different requirements, for instance throughput, power consumption and so on. \glspl{hsa} result to be the most suitable architectures for such a scenario, since their nature allows them to satisfy different goals (e.g. throughput, power and energy consumption, etc.). Therefore, a self-adaptive management of \gls{hsa} resources capable of dealing with various and dynamic workloads may be the right approach to tackle this problem.\\
In the following subsections we will define more in detail the problem we are focusing on. At first, we introduce the kind of applications that this work aims at satisfying (\Cref{subsec:app_models}). Then, we profile such applications and our target device in terms of both power consumption (\Cref{subsec:dvfs_management}) and performance (\Cref{subsec:performance_considerations}), in order to describe the properties they are supposed to feature. Indeed, we expect the power consumption of big and LITTLE clusters to scale as $cV^2f$ (being $c$ a constant typical of each cluster, $V$ the voltage, and $f$ frequency levels), while the applications throughput is assumed to linearly scale as the frequency increases and, since the applications are multi-threaded, as the number of cores enlarges.

\subsection{Application Models}\label{subsec:app_models}
The approach we propose is designed for applications with computational-intensive loops, where the user can specify some minimum throughput constraints on the performance of the loop.
Our reference application model is the one reported in \Cref{fig:app_model}, which is a cyclic task graph where the kernel node, the computationally intensive part of the code, might be potentially parallelized with the fork-join paradigm, by using state-of-the-art libraries, such as OpenMP \cite{OpenMP}.\\
There are many applications that follow such pattern and have been implemented in a parallel way on \glspl{cpu}, \glspl{gpu} or \glspl{fpga}; for instance, financial algorithms, like \gls{bs} benchmark introduced in \Cref{chap:soa}, or video processing ones. Numerical analysis algorithms belong to this category too. An example is \textit{Jacobi iterative method} \cite{jacobi}, which is used to determine the solutions of a diagonally dominant system of linear equations. Another application is \textit{\gls{ssa}} \cite{gillespie, gillespie_new}, an exact procedure for numerically simulating the time evolution of a well-stirred chemically reacting system. Then, \textit{Barnes-Hut algorithm} \cite{barnes_hut} is a physics method for directly computing the force on \textit{N} bodies in the gravitational \textit{N}-body problem. \textit{\gls{rtm}} \cite{rtm} is a geophysics algorithm for seismic imaging that aims at constructing an image of the subsurface from recordings of seismic reflections. Finally,
\textit{Brain Network} image analysis applications exploit the computation of \textit{\gls{pcc}} \cite{pcc}, a statistic coefficient that measures the degree of linear correlation between two variables, to infer interconnections between neurons.\\
This kind of applications may have some throughput requirements that the system has to meet, and they can be expressed as performance requirements over the iterative part of the program. Starting from the model represented in \Cref{fig:app_model}, the user can then express a constraint on the performance of the program included in the \textit{Loop-start} - \textit{Loop-end} nodes of the description.
In this context, we assume that the \textit{Pre-process} and \textit{Post-process} tasks are not computational-intensive and do not impact on application performance.\\
In order to collect the performance information for all the running applications, we assume that high-level information is available and exported by the application.
This means that, instead on relying on low-level metrics such as \gls{ipc}, it is preferable to collect information at an higher abstraction level that is closer to the final user; for instance, we want to express the performance of a video processing application in terms of \textit{frames/s}.
To obtain such a metric, we need to instrument the applications so that they communicate their progress at the end of each iteration (i.e. in the \textit{Loop-end} node).\\
Even though the instrumentation of an application is a invasive task, current libraries such as Application Heartbeats and similar \cite{heartbeats1, heartbeats2, Sironi12} allow for a minimal modification of the application code (a couple of lines of code).
The availability of this high-level information is a benefit not only for the decision mechanism, which can know exactly when an application progresses, but also for the final user who can specify requirements using a meaningful metric.
\begin{figure}
\centering
\includegraphics[scale=0.31]{figures/design/parallel_app}
\caption{Application model}
\label{fig:app_model}
\end{figure}

\subsection{DVFS Management} \label{subsec:dvfs_management}
A first analysis consisted in profiling the power consumption with different \gls{dvfs} levels available for the considered architecture. The power consumption of both big and LITTLE clusters is supposed to scale proportionally to $cV^2f$ expression, where $c$ is a constant typical of each cluster, $V$ the voltage, and $f$ frequency levels.\\
Based on the specific architecture, \gls{dvfs} can be available at the core level, or cluster-level; in the adopted reference platform, the Odroid XU3 development board, this feature is supported at cluster level, by means of \gls{dvfs} actuators and power sensors. This means that, as we set a frequency value for a type of cores (big or LITTLE), this is set for all the cores belonging to that cluster. On the Odroid XU3, Big and LITTLE cores have different frequency ranges: big cores frequency goes from 800MHz to 1900MHz, whereas LITTLE cores frequency from 800MHz to 1300MHz.\\
To collect per-core full power consumption, we used used the Linux \texttt{stress} utility \cite{stress}, which forces a full load on the system and we computed per-core power consumption by dividing the sensed values by the number of used cores. \Cref{tab:odroidPower} reports, per cluster, the power consumed in idle and full power for all the possible \gls{dvfs} configurations; \Cref{fig:odroidPower} illustrates the power curves when only a subset of big or LITTLE cluster is used (the cluster may be distinguish by looking at the reached frequencies).\\
The collected data (which confirm the above power scaling hypothesis) show that the Cortex-A7 cluster used at the highest frequency is generally less power hungry than two Cortex-A15 cores used at the minimum frequency.
Therefore, when running a parallel application, if the LITTLE cores achieve an acceptable performance, it is not necessary to use big cores even if the running applications do compute-intensive operations.
Furthermore, according to the accurate experimental analysis, we can also assume that the power profile of the processor does not significantly vary depending on the application running on the processor, but it depends on the load the application causes on the resource. As a consequence, the power metrics derived from the previous experiment can be used for any application, provided that a scaling factor given by the processor utilization is adopted. These measurements will thus be used to characterize the power consumption of the cores in the \gls{save} Virtual Platform, which registers the system utilization and can provide at each simulation step a power measurement depending on the current workload.

\begin{table}
\centering
\scalebox{0.88}{
\begin{tabular}{ccccc}
\toprule
\textbf{Cluster} & \textbf{Voltage (\texttt{V})} & \textbf{Freq. (\texttt{MHz})} & \textbf{Idle Power (\texttt{W})} & \textbf{Full Power (\texttt{W})} \\
\midrule
\midrule
A7 & 1 & 800 & 0.041 & 0.19 \\
\midrule
A7 & 1 & 900 & 0.059 & 0.232 \\
\midrule
A7 & 1 & 1000 & 0.07 & 0.273 \\
\midrule
A7 & 1.1 & 1100 & 0.083 & 0.319 \\
\midrule
A7 & 1.1 & 1200 & 0.096 & 0.369 \\
\midrule
A7 & 1.2 & 1300 & 0.116 & 0.437 \\
\midrule
\midrule
A15 & 0.9 & 800 & 0.187 & 0.864 \\
\midrule
A15 & 0.9 & 900 & 0.21 & 0.955 \\
\midrule
A15 & 0.9 & 1000 & 0.242 & 1.116 \\
\midrule
A15 & 0.9 & 1100 & 0.278 & 1.284 \\
\midrule
A15 & 1 & 1200 & 0.318 & 1.469 \\ 
\midrule
A15 & 1 & 1300 & 0.362 & 1.731 \\ 
\midrule
A15 & 1 & 1400 & 0.395 & 1.836 \\ 
\midrule
A15 & 1 & 1500 & 0.43 & 2.065 \\
\midrule
A15 & 1 & 1600 & 0.5 & 2.4 \\ 
\midrule 
A15 & 1.1 & 1700 & 0.581 & 2.779 \\ 
\midrule
A15 & 1.1 & 1800 & 0.666 & 3.197 \\
\midrule
A15 & 1.2 & 1900 & 0.8 & 3.985 \\ 
\bottomrule
\end{tabular}
}
\caption{Samsung Exynos 5422 power measurements table}.
\label{tab:odroidPower}
\end{table}

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{figures/design/freq_vs_power_BS}
	\caption{Samsung Exynos 5422 power measurements chart}	\label{fig:odroidPower}
\end{figure}

\subsection{Performance Considerations}\label{subsec:performance_considerations}
\begin{figure}
\centering
\subbottom[Black Scholes benchmark\label{fig:design_bs}]
{
\includegraphics[width=\textwidth]{figures/design/freq_vs_throughput_BS}
}
\subbottom[Pearson Correlation Coefficient benchmark
\label{fig:design_pcc}]
{
\includegraphics[width=\textwidth]{figures/design/freq_vs_throughput_PCC}
}
\caption{Benchmarks throughput as frequency and cores change}
\label{fig:benchmarks_throughput}
\end{figure}
Our methodology does not focus on power only, but aims at meeting also performance requirements. We expect the performance to scale linearly as frequency increases. Moreover, since our target applications are multi-threaded, the performance are supposed to scale also with the number of cores. However, while the power does not depend on the specific application but rather on the load, performance, intended as throughput, depends not only on the specific application, but also on the interaction between different applications concurrently running on the system.
To design an optimization policy it is thus necessary to put performance and power into relation, and rapidly investigate the different possible working points.
\Cref{fig:benchmarks_throughput} reports the performance profile of two of the benchmarks we used on the reference platform. The benchmarks are: \textit{\gls{pcc}} and \textit{\gls{bs}}. Both benchmarks are parallel versions implemented with OpenMP starting from the code available in CUDA \cite{cuda} examples.\\
The measurements have been collected by running the two applications in isolation and we collected the average throughput of each application during its whole execution. For each subset of big/LITTLE cores, we tested all possible frequencies, starting from 800MHz, to big/LITTLE maximum frequency (1900MHz/1300MHz), with a step of 100MHz. For sake of simplicity we report only the curves that use exclusively one cluster.
Such tests confirm the above performance hypothesis, and it illustrates how we can achieve different performance in using the big or the LITTLE cores depending on the application running in the system.
In fact, the \gls{bs} benchmark can benefit from parallelization using the Cortex-A7 cores.
This benchmark scales almost ideally with the number of used cores and, for this reason, it has good performance also on the Cortex-A7 cores.
On the other hand, the \gls{pcc} application distributes the computation unfairly among the available cores and therefore does not obtain an ideal speedup, not performing well on Cortex-A7 cores.\\
Since the proposed policy is tailored for parallel workloads, we can conclude that independently of the actual scalability of the single applications, we will obtain linear dependency on the frequency, and on the number of cores used and their combination. However, this consideration holds as long as we use either big or LITTLE cluster separately. In fact, when we parallelize an application on both clusters, throughput does not scale linearly, but begins to be affected by side effects due to cores synchronizations, in particular when we use different clusters at different frequencies. In such configuration, the ARM big.LITTLE turns into an \gls{aca}, i.e. an architecture where each core (or cluster in this case) is operated at different voltage levels and clock frequencies. \gls{aca} may suffer of performance degradation due to the different frequencies of its cores \cite{benefits_of_big_little}. This means that, paradoxically, a higher frequency may not imply higher throughput. \Cref{fig:bs_throughput_2big_1600} shows how \gls{bs} throughput scales when we use 2 big cores at 1600MHz and 1 to 4 LITTLE cores from 800MHz to 1300MHz. As results, given the number of cores and frequencies, throughput of either big or LITTLE cluster may be estimated, but not when both clusters are employed (or, at least, an estimate will not be 100\% correct).
Finally, we noticed that \gls{hmp} has difficulties in dealing with multi-threaded applications and balancing workload, specially when they are executed on different clusters at different frequencies. An analysis of \gls{hmp} behavior and how to overcome this problem will be presented in \Cref{chap:soa}.\\
In a multiple applications scenario, the Linux scheduler \cite{cfs} tries to assign a fair amount of \gls{cpu} time to applications co-located on the same core, leading to a scalability profile that scales by some factor when two or more applications are co-located, but their profile will not drastically change. In case of parallelization by OpenMP \gls{api}, the workload is fairly distributed among the instantiated threads, even though one application creates more threads than another. For instance, let us consider a benchmark that is run on a Cortex-A15 core. Whether we use one or more threads, the average benchmark throughput will still be the same, let us call it $X$. Now, if we run two instances of the benchmark on the same Cortex-A15 core (the first one creates one thread, the second two threads), the final throughput will be, respectively, $\frac{1}{3}X$ and $\frac{2}{3}X$.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{figures/design/freq_vs_throughput_BS_2_big_1600}
	\caption{Black Scholes throughput using 2 big cores at 1600MHz}	\label{fig:bs_throughput_2big_1600}
\end{figure}

\section{Proposed Solution}
\label{sec:proposed_solution}
The overall idea of the run-time resource management policy we propose is to combine the information regarding power and performance scaling, reported above, to derive a method to explore quickly and effectively the search space at run-time. Moreover, the policy distributes the workload by mapping threads to the available cores according to a mapping ratio. Such value is derived as the ratio between the big cores throughput and LITTLE cores throughput at their given frequencies. Thus, a proper workload distribution ensures higher application performance. Such mapping strategy will be defined in \Cref{chap:soa}. Finally, our policy can also leverage the availability of online performance estimation obtained by means of an \textit{Application Heartbeat \gls{api}}.

\subsection{Configuration Space}\label{subsec:configuration_definition}
The policy we propose is aimed at managing both \gls{dvfs} and the number of cores currently used by the system exploiting mechanisms to enable/disable cores as the ones exposed by the Linux \textit{sysfs} file system \cite{mauerer}.
In this context the possible configurations of our system can be represented by the tuple $<N_B,f_B,N_L,f_L>$, as we did in \Cref{chap:soa}. The dimension of the configuration space is:\\
\begin{align*}
conf_B = |N_B| \times |f_B| = 4 \times 12 = 48
\end{align*}
\begin{align*}
conf_L = |N_L| \times |f_L| = 4 \times 6 = 24
\end{align*}
\begin{align*}
conf_{BL} = |N_B| \times |f_B| \times |N_L| \times |f_L| = 4 \times 12 \times 4 \times 6 = 1152
\end{align*}
\begin{align*}
conf_{B} + conf_{L} + conf_{BL} = 48 + 24 + 1152 = 1224
\end{align*}
\\
Note that, in this computation, we assumed that the \gls{dvfs} controller sets the frequency for the whole cluster and not for the single core, according to the current ARM big.LITTLE architecture, and that there is no difference in the specific combination of selected active cores, which is generally true.
The configuration space so derived is large (1224 different configurations), but not cumbersome; these information can be stored in the system memory, but we still need some smart method to navigate this configuration space in search for the right one to use to guarantee both throughput requirements and power efficiency.

\subsection{Power Characterization}\label{subsec:power_characterization}
A first aspect to consider is that each one of these configurations is characterized by its power consumption, which is given by the sum of the power consumption of the used cores. The power consumption of given configuration $<N_B, f_B, N_L, f_L>$ is:
\\
\begin{align*}
idle_{B} = 4 - N_B
\end{align*}
\begin{align*}
idle_{L} = 4 - N_L
\end{align*}
\begin{align*}
power_{B} = fullPower_{B}@f_B \times N_B + idlePower_{B}@f_B \times idle_{B}
\end{align*}
\begin{align*}
power_{L} = fullPower_{L}@f_L \times N_L + idlePower_{L}@f_L \times idle_{L}
\end{align*}
\begin{align*}
totalPower = power_{B} + power_{L}
\end{align*}
\\
The power information exploited in these formulas come from the previous power profiling.
\subsection{Performance Characterization}\label{subsec:performance_characterization}
Regarding the performance, we can also suppose that each one of these configurations is characterized by a given performance and that the following assumptions holds:
\begin{itemize}
\item application performance benefits from a fairly balanced distribution among used cores;
\item application performance generally improves with the number of cores on which computations are parallelized;
\item given two different processor types, it is possible to measure the performance gain when switching from one
to the other.
\end{itemize}

Under this considerations we can characterize each configuration with a performance value, in terms of a speedup with respect to a base configuration.
Let us suppose we know how the performance of the system scales as we add a Cortex-A15 (Cortex-A7) core or we switch core frequency. We define such factors as $coreScale_B$ ($coreScale_L$) and $freqScale_B$ ($freqScale_L$). Such factors are approximated values extracted during performance analysis tests. Assuming linear scaling and no influence we can compute the speedup with simple formulas. For instance, the speedup expected for the configuration $<N_B, f_B, N_L, f_L>$ can be computed as:
\\
\begin{align*}
speedup_{B} = N_B \times coreScale_B + \frac{f_B - minf_B}{100} \times freqScale_B \times N_B
\end{align*}
\begin{align*}
speedup_{L} = N_L \times coreScale_L + \frac{f_L - minf_L}{100} \times freqScale_L \times N_L
\end{align*}
\begin{align*}
totalSpeedup = speedup_{B} + speedup_{L}
\end{align*}
\\
Obviously this is an ideal speedup model, which is not 100\% accurate, in particular due to the not monotonic behavior of throughput in configurations that employ both big and LITTLE cores. A more accurate model would be too tailored to the benchmark used as reference, and so useless in case of different benchmarks. Moreover, this speedup value is characteristic of every application (when running alone), and it depends on how the applications influence each others when they are co-located.
Therefore, it would be useless to profile offline the applications to determine the scale factors to use to setup the policy.
For this reason, in our methodology we adapt the speedups relying on online performance measurements and we constantly update those values depending on the current running conditions (i.e. the application mix); \Cref{chap:implementation} details the update process.
However, even if we change online the speedups, our speedup model is monotone; hence configurations can consequently be ordered on the basis of the speedup or the power. Although this may be correct for power consumption, it does not hold for speedup, as we previously stated. Therefore, our policy must be aware of the fact that, given a configuration, a following one, in terms of speedup, might not entail higher throughput.

\subsection{Policy Overview}
\label{subsec:policy_heart}
In this section we provide an overview of our proposed solution. Our policy is targeted for asymmetric processors and introduces the advantage not only to control \gls{dvfs}, but, taking care of heterogeneity, also to control the number of currently active cores (big or LITTLE) to achieve better performance at power and throughput ends.\\
\Cref{fig:policy_steps} shows the workflow of our policy. Now we briefly describe each Step, while a more detailed analysis is performed in \Cref{chap:implementation}.
\begin{description}
\item[Step 1:] the policy starts by building a table with all the possible available configurations, their power consumption values and speedup values.
\item[Step 2:] the policy profiles the applications in two reference configurations, in order to generate a reference mapping ratio. Such value will then be used to generate the mapping ratios for other configurations.
\item[Step 3:] the policy chooses a default operation point to be enforced as initial starting configuration and begins to monitor the applications execution. At each monitoring iteration, the policy computes the needed speedup with respect to the applications performance, and updates the baseline.
\item[Step 4:] the policy scans the configuration table to find the optimal configuration, i.e. the one that delivers the needed speedup. The scan is implemented as a variant of binary search.
\item[Step 5:] when the policy converges, it scans all the configurations that guarantee the \gls{qos} (i.e. all the configurations following the one the policy has converged to) to find the most power efficient configuration.
\item[Step 6:] the policy actuates the configuration by changing the affinity mask of all the applications in order to use the same group of cores and changing the operating frequency of the used computational resources.
\item[Step 7:] when the application set changes, a reset routine is used to restore some parameters to their default values so that the policy can converge again to a convenient configuration for the new application set.
\end{description}

\begin{figure}
\centering
\includegraphics[scale=0.5]{figures/implementation/policy_steps}
\caption{Policy workflow}
\label{fig:policy_steps}
\end{figure}