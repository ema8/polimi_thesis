\chapter{HMP Analysis} \label{chap:hmp_analysis}

In this chapter, we provide an analysis of \gls{hmp} scheduler. The knowledge of the behavior and scalability of multi-threaded applications may be used to enhance performance, in terms of throughput, power and energy consumption of such applications. 
For this reason, we ran several tests on the Odroid XU3 in order to profile the behavior of \gls{hmp} scheduler in case of multi-threaded applications, i.e. the applications we want to target. \Cref{sec:throughput_analysis}
presents the throughput analysis of a multi-threaded application. \Cref{sec:thread_mapping} is about the first prototype of our solution we found to both balance the workload among the cores and improve performance. \Cref{sec:enhancing_thread_mapping} shows two attempts of enhancement for our solution. Finally, \Cref{sec:conclusions} reviews the analysis we performed on \gls{hmp} scheduler. 

\section{Throughput Analysis} \label{sec:throughput_analysis}
For these tests, we will focus on \gls{bs} \cite{black_scholes} benchmark, i.e. an implementation of Black and Scholes model, a mathematical model of financial market, widely used in global financial markets to calculate the theoretical price of European options (a type of financial security). We profiled \gls{bs} benchmark in different configurations. We define a configuration as a tuple $<N_B,f_B,N_L,f_L>$, where: 
\begin{enumerate}[label= \arabic*)]
\item $N_B$ is the number of used big cores,
\item $f_B$ is the frequency of big cores,
\item $N_L$ is the number of  used LITTLE cores,
\item $f_L$ is the frequency of LITTLE cores.
\end{enumerate}
We tested how \gls{bs} scales as the number of cores and frequency change. In particular, we focused only on configurations that use only either big cores or LITTLE cores. In this way, we wanted to find a ratio between big and LITTLE throughput performance. For each subset of big/LITTLE cores, we tested all possible frequencies, starting from 800MHz, to big/LITTLE maximum frequency (1900MHz/1300MHz), with a step of 100MHz. Hence, the dimension of the configuration space we analyzed is:
\\
\begin{align*}
|N_B| \times |f_B| + |N_L| \times |f_L| = 4 \times 12 + 4 \times 6 = 72
\end{align*}
\\
The command \texttt{cpufreq-set} \cite{cpufreq-set} allows to change the frequency of either big or LITTLE cores. On the other hand, the command \texttt{taskset} \cite{taskset} allows to set or retrieve the \gls{cpu} affinity of a new command or a running one. Hence, we can decide on which core we want to run an application.\\
\gls{bs} kernel is performed 300 times, while each kernel instance executes 4000000 options. \gls{bs} benchmark is parallelized by OpenMP \glspl{api}; in particular, OpenMP uses 8 threads by default for each kernel execution. As we stated in \Cref{chap:design}, the results of our tests showed that \gls{bs} is a well-balanced benchmark, which scales almost ideally with the number of used cores (when we employ only one cluster at the time). In \Cref{fig:black_scholes_performance} we reported again \gls{bs} performance chart.\\
We can notice that 1 big core at 1900MHz, ideally, performs as 2 LITTLE cores at 1200MHz. As well, 2 big cores at 1900MHz ideally perform as 4 LITTLE at 1100MHz. Therefore we can use this information in order to have a better mapping of threads on big and LITTLE cores. In particular, for each thread we map on a LITTLE core, we should map 2 threads on a big core.\\
Finally, when we tested \gls{bs} on all the other configurations, we noticed weird behavior of \gls{hmp} in all the configurations of this form: $<1, f_B, N_L, f_L>$, with $N_L \geq 1$. In particular, although a number from 1 to 4 of LITTLE cores was available, \gls{hmp} scheduled the task on the big core only. On the other hand, in all the other configurations different from $<1, f_B, N_L, f_L>$, \gls{hmp} performs properly. Such final analysis was carried out using process and resource viewers, like \textbf{htop} \cite{htop}. This unexpected behavior suggested us that \gls{hmp} has difficulties in dealing with multi-threaded applications, in particular when they are parallelized on big and LITTLE cores at different frequencies. Our idea is to enhance our run-time resource management policy in order to balance workload by properly spread the instantiated threads among the cores according to a mapping ratio.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{figures/hmp_analysis/freq_vs_throughput_BS}
\caption{Black Scholes throughput as frequency and cores change}
\label{fig:black_scholes_performance}
\end{figure}

\section{Thread Mapping} \label{sec:thread_mapping}
We have seen so far that it is possible to find a ratio between big and LITTLE cores throughput performance. In \gls{bs} case, this ratio is close to 2. Hence, we want to use such value and manually map threads to big/LITTLE cores. Specifically, when we map 1 thread on a LITTLE core, we map 2 threads on a big core. The default number of threads available for our architecture is 8. When a perfect mapping is not available, we tried different mappings in order to find the most suitable for that configuration. Linux provides \glspl{api} able to set the affinity of a thread to a certain core. The command is \mbox{\texttt{sched\_setaffinity(tid, sizeof(set), \&set)}} \cite{schedsetaffinity}, where:
\begin{itemize}
\item \texttt{tid} is the thread ID,
\item \texttt{set} is a variable of type \texttt{cpu\_set\_t} and represents the core the thread has to be mapped to.
\end{itemize}
For our tests, we set core frequency to 1900MHz for big cores and 1300MHz for LITTLE cores. This because the frequencies of LITTLE cores we observed having similar behavior, in terms of throughput, to big cores vary. Hence, in first approximation, we decided to set LITTLE cores frequency to their maximum, as well as big cores frequency.\\
The configurations we are going to consider in these tests are the ones of this kind: $<N_B, 1900, N_L, 1300>$. In particular, we split them in two subsets:
\begin{itemize}
\item \emph{Critical Configurations}, defined by the tuple:\newline
$<1, 1900, N_L, 1300>$, with $N_L \geq 1$,
\item \emph{Generic Configurations}, defined by the tuple:\newline
$<N_B, 1900, N_L, 1300>$, with $N_B > 1$ and $N_L \geq 1$.
\end{itemize}
The former are the configurations where we noticed that \gls{hmp} does not spread the workload over the cores available, the latter are the ones where \gls{hmp} works properly. For each subset of configuration, we test \gls{hmp} versus our solution, which means manually mapping threads to big/LITTLE cores, with respect to the ratio we previously found. Finally, we compare the results of our tests in terms of throughput, measured as \gls{SPS}, execution time (second), power consumption (watt), energy consumption (joule), and, finally, throughput over energy consumption (\gls{SPS} / joule) as a cumulative measure of all the previous metrics. Power and energy consumption measures represent the consumption of the whole system.

\subsection{Critical Configurations Tests} \label{subsec:critical_configuration_tests}
Here we present the tests run on Critical Configurations, i.e. configurations whose form is $<1, 1900, N_L, 1300>$, where $N_L \geq 1$. In such configurations, \gls{hmp} does not perform in a proper way. In other words, \gls{hmp} does not parallelize the workload over the cores available, but it runs the task on the big core only. As consequence, the application throughput is lower than what it could actually be. For this reason, we want to see if, manually allocating threads on cores, we can improve the application throughput.

\subsubsection{1 big core - 3 LITTLE cores} \label{subsubsec:1_big_core_3_LITTLE_cores}
In configuration $<1, 1900, 3, 1300>$, since a perfect mapping was not possible, we tried two mappings:
\begin{enumerate}[label= \arabic*)]
\item 2 threads on 1 big core, 2 threads on each LITTLE core,
\item 4 threads on 1 big core, 2 threads on 1 LITTLE core, and 1 thread on each of the remaining 2 LITTLE cores.
\end{enumerate}
The results of the tests are the following:
\begin{description}
\item[throughput:] both our solutions surpass \gls{hmp} scheduler in terms of throughput (\Cref{fig:1big_3little_performance}). Specifically, \gls{hmp} throughput, on average, is $8.28 \times 10^5$, while our mappings throughput are, respectively, $20.17 \times 10^5$ and $16.46 \times 10^5$. This means that, in this Critical Configuration, one of our mappings can achieve a 2.44X speedup.
\item[power consumption:] \gls{hmp} scheduler is not the most power efficient solution (\Cref{fig:1big_3little_power}); indeed, our first mapping consumes, on average, \texttt{2.07W}, our second mapping \texttt{2.29W}, and, finally, \gls{hmp} \texttt{2.19W}. If we consider the execution time, \gls{hmp} is the slowest solution (\texttt{374s}), whereas our mappings take \texttt{154s} and \texttt{189s}.
\item[energy consumption:] thanks to their execution times and, for the first mapping, also its power consumption, the energy required by our mappings is quite smaller than \gls{hmp} (\Cref{fig:1big_3little_energy}). In particular, \gls{hmp} energy consumption is \texttt{1638.30J}, instead our mappings consume, respectively, \texttt{642.47J} and \texttt{869.38J}.
\item[throughput-energy ratio:] again, the ratio shows that our solutions are overall more efficient than \gls{hmp} (\Cref{fig:1big_3little_throughput_energy}). The ratio values are, for our mappings, 3139.87 and 1893.39, while, for \gls{hmp}, 505.04.
\end{description}

\clearpage
\begin{figure*}
\centering
\subbottom[1 big - 3 LITTLE throughput\label{fig:1big_3little_performance}]
{
\includegraphics[width=\textwidth]{figures/hmp_analysis/1big_3little_performance}
}
\subbottom[1 big - 3 LITTLE power consumption
\label{fig:1big_3little_power}]
{
\includegraphics[width=\textwidth]{figures/hmp_analysis/1big_3little_power}
}
\label{fig:1big_3little_test}
\caption{1 big - 3 LITTLE test}
\end{figure*}

\begin{figure*}
\centering
\contsubbottom[1 big - 3 LITTLE energy consumption
\label{fig:1big_3little_energy}]
{
\includegraphics[width=\textwidth]{figures/hmp_analysis/1big_3little_energy}
}
\contsubbottom[1 big - 3 LITTLE throughput-energy ratio
\label{fig:1big_3little_throughput_energy}]
{
\includegraphics[width=\textwidth]{figures/hmp_analysis/1big_3little_throughput_energy}
}
\contcaption{1 big - 3 LITTLE test}
\end{figure*}
\clearpage

\subsubsection{Summary}  \label{subsubsec:critical_summary}
We have seen that \gls{hmp} scheduler has a strange behavior in the Critical Configuration we tested. The same behavior can be observed in Critical Configurations $<1, 1900, 1, 1300>$, $<1, 1900, 2, 1300>$ and $<1, 1900, 4, 1300>$. \Cref{fig:1big_1little-1big_2little_test} shows the same tests performed on Critical Configurations $<1, 1900, 1, 1300>$, $<1, 1900, 2, 1300>$, while the results of tests on Critical Configuration $<1, 1900, 4, 1300>$ are reported at the end of the chapter in \Cref{tab:tests_summary}, as well as all the other tests results.\\
In all the cases, our thread mappings result in better throughput (around 2X speedup in most of the cases). Although some mappings consume few hundred \texttt{mWatts} more than \gls{hmp}, this does not impact the overall energy consumption since execution time of our mappings is impressively shorter than \gls{hmp}. As consequence, our solutions energy consumption is remarkably smaller than \gls{hmp}. As well, throughput-energy ratio, which indicates the goodness of one solution with respect to another, always promotes our thread mapping against \gls{hmp}.\\
It is interesting to highlight that \gls{hmp} throughput, execution time, power consumption, energy consumption, and throughput-energy ratio almost do not change in all the 4 configurations we tested. Just configuration $<1, 1900, 4, 1300>$ has slightly better throughput. This means that having 1 to 4 LITTLE cores available, in addiction to 1 big core, makes quite no difference to \gls{hmp} scheduler.

\clearpage
\begin{figure*}
\centering
\subbottom[1 big - 1 LITTLE and 1 big - 2 LITTLE throughput\label{fig:1big_1little-1big_2little_performance}]
{
\includegraphics[width=\textwidth]{figures/hmp_analysis/1big_1little-1big_2little_performance}
}
\subbottom[1 big - 1 LITTLE and 1 big - 2 LITTLE power consumption
\label{fig:1big_1little-1big_2little_power}]
{
\includegraphics[width=\textwidth]{figures/hmp_analysis/1big_1little-1big_2little_power}
}
\caption{1 big - 1 LITTLE and 1 big - 2 LITTLE tests}
\end{figure*}

\begin{figure*}
\centering
\contsubbottom[1 big - 1 LITTLE and 1 big - 2 LITTLE energy consumption
\label{fig:1big_1little-1big_2little_energy}]
{
\includegraphics[width=\textwidth]{figures/hmp_analysis/1big_1little-1big_2little_energy}
}
\contsubbottom[1 big - 1 LITTLE and 1 big - 2 LITTLE throughput-energy ratio
\label{fig:1big_1little-1big_2little_throughput_energy}]
{
\includegraphics[width=\textwidth]{figures/hmp_analysis/1big_1little-1big_2little_throughput_energy}
}
\contcaption{1 big - 1 LITTLE and 1 big - 2 LITTLE tests}
\label{fig:1big_1little-1big_2little_test}
\end{figure*}
\clearpage

\section{Enhancing Thread Mapping} \label{sec:enhancing_thread_mapping}
We performed tests on Generic Configurations $<2, 1900, 2, 1300>$ and $<4, 1900, 4, 1300>$ (the results are reported in \Cref{tab:tests_summary}). From these tests, it is evident that, when we are dealing with Generic Configurations, \gls{hmp} performs better than our thread mapping solution, in terms of both throughput and execution time, although our mappings power consumption is smaller than \gls{hmp}, as well as energy consumption (for some mappings). Hence, we need to improve our mapping solution if we want to deal with Generic Configurations.\\
In this section, we present the enhancements we introduced in our thread mapping solution in order to improve our performance, particularly in terms of throughput. In \Cref{subsec:perfect_mapping}, we tested two Generic Configurations: $<2, 1900, 2, 1300>$ and $<3, 1900, 2, 1300>$. The goal was to have a perfect mapping according to our ratio. Therefore, when it was necessary, we changed the number of usable threads.\\
Finally, we also tried to exploit OpenMP dynamic scheduler in order to check if we could improve the throughput of our perfect mappings. While OpenMP static scheduler divides the loop into equal-sized chunks, or, at least, as equal as possible, OpenMP dynamic scheduler exploits internal work queue to assign each thread a chunk-sized block of the loop iterations. Besides, at the end of a thread execution, the thread pops the next block from the top of the work queue. Since this solution did not provide an improvement in terms of the metrics we are considering, the results of such tests are directly reported in \Cref{tab:tests_summary}.

\subsection{Perfect Mapping} \label{subsec:perfect_mapping}
Here, we present the perfect mapping tests run on Generic Configurations $<2, 1900, 2, 1300>$ and $<3, 1900, 2, 1300>$, where we used a perfect mapping of threads on the available cores, according to the ratio we found for \gls{bs} benchmark. While in the former configuration we changed the number of usable threads, the latter configuration was already suitable for this goal.

\subsubsection{2 big cores - 2 LITTLE cores} \label{subsubsec:perfect_2_big_cores_2_LITTLE_cores}
Since for configuration $<2, 1900, 2, 1300>$ a perfect mapping with 8 threads was not available, we set the number of usable threads to 6. Hence, we ran the tests with the following schedules:
\begin{enumerate}[label= \arabic*)]
\item \gls{hmp} with 8 and 6 usable threads,
\item 2 threads on each big core, 1 threads on each LITTLE core.
\end{enumerate}
The results of the perfect mapping test for the Generic Configuration $<2, 1900, 2, 1300>$ are the following:
\begin{description}
\item[throughput:] 6-threads \gls{hmp} and our perfect mapping have, on average, throughput almost identical to 8-threads \gls{hmp} (\Cref{fig:2big_2little_performance_perfect}). In particular, while 8-threads \gls{hmp} throughput is $24.73 \times 10^5$, 6-threads \gls{hmp} throughput is $24.50 \times 10^5$, and our perfect mapping throughput is $24.69 \times 10^5$. In terms of slowdown, for 6-threads \gls{hmp} it is 0.991X, for our perfect mapping it is 0.999X.
\item[power consumption:] both 6-threads \gls{hmp} and our perfect mapping power consumption are slightly smaller than 8-threads \gls{hmp} (\Cref{fig:2big_2little_power_perfect}); indeed, while 8-threads \gls{hmp} power consumption is \texttt{3.48W}, 6-threads \gls{hmp} requires, on average, \texttt{3.46W}, and our perfect mapping averagely consumes \texttt{3.45W}, which is higher than the power consumed by the previous mappings for this Generic Configuration. If we consider the execution time, 8-threads \gls{hmp} is still the best solution (\texttt{122s}), while the time needed by 6-threads \gls{hmp} and our perfect mapping to complete the benchmark is nearly the same (respectively, \texttt{127s} and \texttt{126s}). The execution time of our perfect mapping is definitely shorter than the one required by our previous mappings.
\item[energy consumption:] since the power consumption our the three solutions we are considering in this test are very similar, the energy consumption mainly depends on the execution time (\Cref{fig:2big_2little_energy_perfect}). As consequence, 8-threads \gls{hmp} is still the most energy efficient solution (\texttt{856.06J}), instead 6-threads \gls{hmp} is the most energy-hungry solution (\texttt{886.28J}). Finally our perfect mapping consumes \texttt{872.39J}, and, due to the higher power consumption, such value is higher than the energy consumed by the previous mappings.
\item[throughput-energy ratio:] again, 8-threads \gls{hmp} results to be the most efficient schedule, according to this metric (\Cref{fig:2big_2little_throughput_energy_perfect}). On one hand, 8-threads \gls{hmp} throughput-energy ratio is 2888.39, on the other, 6-threads \gls{hmp} ratio value is 2763.97, and our perfect mapping ratio value is 2830.06, an enhancement with respect to the previous mappings.
\end{description}
This test shows that, when we can exploit a perfect thread mapping, our solution is capable of reaching and overtaking \gls{hmp} performance. Indeed, our perfect mapping solution is superior to 6-threads \gls{hmp} solution in each metric we considered, even though some values are very close. On the other hand, 8-threads \gls{hmp} has still better performance than our solution, although our perfect mapping performance are similar to 8-threads \gls{hmp} than our previous mappings for this Generic Configuration.

\clearpage
\begin{figure*}
\centering
\subbottom[Perfect mapping 2 big - 2 LITTLE throughput\label{fig:2big_2little_performance_perfect}]
{
\includegraphics[width=\textwidth]{figures/hmp_analysis/2big_2little_performance_perfect}
}
\subbottom[Perfect mapping 2 big - 2 LITTLE power consumption
\label{fig:2big_2little_power_perfect}]
{
\includegraphics[width=\textwidth]{figures/hmp_analysis/2big_2little_power_perfect}
}
\label{fig:2big_2little_test_perfect}
\caption{Perfect mapping 2 big - 2 LITTLE test}
\end{figure*}

\begin{figure*}
\centering
\contsubbottom[Perfect mapping 2 big - 2 LITTLE energy consumption
\label{fig:2big_2little_energy_perfect}]
{
\includegraphics[width=\textwidth]{figures/hmp_analysis/2big_2little_energy_perfect}
}
\contsubbottom[Perfect mapping 2 big - 2 LITTLE throughput-energy ratio
\label{fig:2big_2little_throughput_energy_perfect}]
{
\includegraphics[width=\textwidth]{figures/hmp_analysis/2big_2little_throughput_energy_perfect}
}
\contcaption{Perfect mapping 2 big - 2 LITTLE test}
\end{figure*}
\clearpage

\subsubsection{3 big cores - 2 LITTLE cores} \label{subsubsec:perfect_3_big_cores_2_LITTLE_cores}
Configuration $<3, 1900, 2, 1300>$ provides a perfect thread mapping, which is: 2 threads on each big core, 1 thread on each LITTLE core.\\
Here, we introduce the results of the tests for Generic Configuration $<3, 1900, 2, 1300>$:
\begin{description}
\item[throughput:] differently from previous Generic Configuration tests, our perfect mapping is able to surpass \gls{hmp} performance in terms of throughput (\Cref{fig:3big_2little_performance_perfect}). Specifically, \gls{hmp} throughput is, on average, $28.75 \times 10^5$, whereas our perfect mapping average throughput is $32.74 \times 10^5$.
\item[power consumption:] \gls{hmp} results to be more power efficient than our perfect mapping solutions (\Cref{fig:3big_2little_power_perfect}); indeed, \gls{hmp} power consumption is averagely \texttt{4.55W}, while our perfect mapping consumes little more power (\texttt{4.63W}). In terms of execution time, our perfect mapping results to be faster than \gls{hmp}. (\texttt{95s} versus \texttt{108s}).
\item[energy consumption:] Our perfect mapping results to be more energy efficient than \gls{hmp} solution (\Cref{fig:3big_2little_energy_perfect}). In particular, \gls{hmp} consumes \texttt{986.98J}, while the energy consumption of our perfect mapping is \texttt{888.39J}. This is due to the difference in the execution times, since the power consumption are quite close.
\item[throughput-energy ratio:] this metric suggests that the most efficient schedule is our perfect mapping solution (\Cref{fig:3big_2little_throughput_energy_perfect}). The throughput-energy ratio value of our perfect mapping is 3685.14, while the value of \gls{hmp} scheduler is 2913.27.
\end{description}

\clearpage
\begin{figure*}
\centering
\subbottom[Perfect mapping 3 big - 2 LITTLE throughput\label{fig:3big_2little_performance_perfect}]
{
\includegraphics[width=\textwidth]{figures/hmp_analysis/3big_2little_performance_perfect}
}
\subbottom[Perfect mapping 3 big - 2 LITTLE power consumption
\label{fig:3big_2little_power_perfect}]
{
\includegraphics[width=\textwidth]{figures/hmp_analysis/3big_2little_power_perfect}
}
\label{fig:3big_2little_test_perfect}
\caption{Perfect mapping 3 big - 2 LITTLE test}
\end{figure*}

\begin{figure*}
\centering
\contsubbottom[Perfect mapping 3 big - 2 LITTLE energy consumption
\label{fig:3big_2little_energy_perfect}]
{
\includegraphics[width=\textwidth]{figures/hmp_analysis/3big_2little_energy_perfect}
}
\contsubbottom[Perfect mapping 3 big - 2 LITTLE throughput-energy ratio
\label{fig:3big_2little_throughput_energy_perfect}]
{
\includegraphics[width=\textwidth]{figures/hmp_analysis/3big_2little_throughput_energy_perfect}
}
\contcaption{Perfect mapping 3 big - 2 LITTLE test}
\end{figure*}
\clearpage

\subsubsection{Summary} \label{subsubsec:perfect_summary}
These tests proved that, when a perfect mapping is available (thanks to the configuration or setting the number of usable threads), it is possible to achieve at least the same throughput of 8-threads \gls{hmp} scheduler.\\
In Generic Configuration $<2, 1900, 2, 1300>$, our perfect mapping reached, on average, performance very similar to 8-threads \gls{hmp}, in terms of throughput and power consumption. On the other hand, execution time and energy consumption of 8-threads \gls{hmp} are slightly better than our perfect mapping ones. If we compare our solution with 6-threads \gls{hmp}, it results to have better performance in all metrics. Finally, in throughput-energy ratio metric, our perfect mapping performance is very close to 8-threads \gls{hmp}, whereas it surpasses 6-threads \gls{hmp}.\\
General Configuration $<3, 1900, 2, 1300>$ did not require a change in the number of available threads, since it already provided a perfect mapping. Our solution performs certainly better than \gls{hmp} in terms of throughput, execution time and energy consumption, while just the power consumption is marginally higher than \gls{hmp}. Therefore, also throughput-energy ratio of our perfect mapping is superior to \gls{hmp} ratio.

\section{Conclusions} \label{sec:conclusions}
We have seen so far that it is possible to improve the performance of \gls{hmp}, the Odroid XU3 heterogeneous scheduler. In particular, our solution is manually mapping  threads to cores according to a ratio between big and LITTLE cores we found analyzing how \gls{bs} benchmark scales as the number of cores and frequency change.\\
We tested two kinds of configurations: Critical and Generic Configurations. In the former case, we took advantage of \gls{hmp} improper behavior, and we always achieved great results. In the latter case, at first, \gls{hmp} performance was definitely better than our thread mapping solution. Here \gls{hmp} properly spreads the workload over the available cores. Besides, the Generic Configurations we tested did not provide a perfect mapping. Hence, we either changed the number of usable threads or chose a suitable configuration in order to achieve a perfect mapping. In this way, we improved our solution throughput performance and reached, at least, 8-threads \gls{hmp} throughput. Finally, we exploited OpenMP features to further progress our solution performance, but we did not achieve the desired results. \Cref{tab:tests_summary} collects the results of all the tests we ran. The speedup is computed with respect to the 8-threads not-dynamic \gls{hmp} schedule of each configuration.\\
The limitations of \gls{hmp} scheduler we presented in this chapter will be used as starting point for the development of our proposed solution, i.e. a workload-aware run-time resource management policy able to find a convenient configuration for the application goal, automatically compute the mapping ratio, and finally distribute the workload. 

\begin{landscape}
\begin{table}
\centering
\scalebox{0.6}{
\begin{tabular}{ccccccccc}
\toprule
\textbf{Configuration} & \textbf{Scheduler/Mapping} & \textbf{Threads} & \textbf{Speedup} & \textbf{Throughput (\texttt{M\gls{SPS}})} & \textbf{Time (\texttt{s})} & \textbf{Power (\texttt{W})} & \textbf{Energy (\texttt{J})} & $\frac{\textbf{Throughput}}{\textbf{Energy}}$  $(\frac{\textbf{\texttt{{sps}}}}{\textbf{\texttt{J}}})$\\
\midrule
\midrule
$<1, 1900, 1, 1300>$ & HMP & 8 & 1X & 0.828 & 370 & 2.15 & 1596.80 & 518.51\\
\midrule
$<1, 1900, 1, 1300>$ & 5$_{big}$ 3$_{LITTLE}$ & 8 & 1.59X & 1.316 & 233 & 2.25 & 1051.57 & 1251.63\\
\midrule
$<1, 1900, 1, 1300>$ & 6$_{big}$ 2$_{LITTLE}$ & 8 & 1.33X & 1.097 & 277 & 2.18 & 1213.54 & 904.01\\
\midrule
\midrule
$<1, 1900, 2, 1300>$ & HMP & 8 & 1X & 0.828 & 368 & 2.15 & 1586.58 & 521.74\\
\midrule
$<1, 1900, 2, 1300>$ & 4$_{big}$ 2-2$_{LITTLE}$ & 8 & 1.97X & 1.644 & 187 & 2.31 & 865.23 & 1900.55\\
\midrule
\midrule
$<1, 1900, 3, 1300>$ & HMP & 8 & 1X & 0.828 & 374 & 2.19 & 1638.30 & 505.04\\
\midrule
$<1, 1900, 3, 1300>$ & 2$_{big}$ 2-2-2$_{LITTLE}$ & 8 & 2.44X & 2.017 & 154 & 2.07 & 642.47 & 3139.87\\
\midrule
$<1, 1900, 3, 1300>$ & 4$_{big}$ 2-1-1$_{LITTLE}$ & 8 & 1.99X & 1.646 & 189 & 2.29 & 869.38 & 1893.39\\
\midrule
\midrule
$<1, 1900, 4, 1300>$ & HMP & 8 & 1X & 0.918 & 368 & 2.15 & 1587.09 & 578.62\\
\midrule
$<1, 1900, 4, 1300>$ & 4$_{big}$ 1-1-1-1$_{LITTLE}$ & 8 & 1.77X & 1.641 & 186 & 2.31 & 864.17 & 1898.39\\
\midrule
\midrule
$<2, 1900, 2, 1300>$ & HMP & 8 & 1X & 2.473 & 122 & 3.48 & 856.06 & 2888.39\\
\midrule
$<2, 1900, 2, 1300>$ & 2-2$_{big}$ 2-2$_{LITTLE}$ & 8 & 0.82X & 2.020 & 152 & 2.70 & 827.02 & 2442.88\\
\midrule
$<2, 1900, 2, 1300>$ & 3-3$_{big}$ 1-1$_{LITTLE}$ & 8 & 0.89X & 2.195 & 141 & 3.37 & 953.52 & 2301.52\\
\midrule
$<2, 1900, 2, 1300>$ & HMP & 6 & 0.991X & 2.450 & 127 & 3.46 & 886.28 & 2763.97\\
\midrule
$<2, 1900, 2, 1300>$ & 2-2$_{big}$ 1-1$_{LITTLE}$ & 6 & 0.999X & 2.469 & 126 & 3.45 & 872.39 & 2830.06\\
\midrule
$<2, 1900, 2, 1300>$ & dynamic HMP & 8 & 0.56X & 1.376 & 222 & 3.21 & 1430.30 & 962.54\\
\midrule
$<2, 1900, 2, 1300>$ & dynamic HMP & 6 & 0.55X & 1.367 & 223 & 3.20 & 1429.48 & 956.06\\
\midrule
$<2, 1900, 2, 1300>$ & dynamic 2-2$_{big}$ 1-1$_{LITTLE}$ & 6 & 0.80X & 1.980 & 154 & 3.38 & 1043.21 & 1898.10\\
\midrule
\midrule
$<3, 1900, 2, 1300>$ & HMP & 8 & 1X & 2.875 & 108 & 4.55 & 986.98 & 2913.27\\
\midrule
$<3, 1900, 2, 1300>$ & 2-2-2$_{big}$ 1-1$_{LITTLE}$ & 8 & 1.14X & 3.274 & 95 & 4.63 & 888.39 & 3685.14\\
\midrule
$<3, 1900, 2, 1300>$ & dynamic HMP & 8 & 0.72X & 2.071 & 149 & 4.37 & 1310.16 & 1580.90\\
\midrule
$<3, 1900, 2, 1300>$ & dynamic 2-2-2$_{big}$ 1-1$_{LITTLE}$ & 8 & 0.90X & 2.597 & 119 & 4.59 & 1096.14 & 2369.11\\
\midrule
\midrule
$<4, 1900, 4, 1300>$ & HMP & 8 & 1X & 4.422 & 72 & 5.98 & 873.69 & 5061.40\\
\midrule
$<4, 1900, 4, 1300>$ & 1-1-1-1$_{big}$ 1-1-1-1$_{LITTLE}$ & 8 & 0.91X & 4.015 & 82 & 4.18 & 693.23 & 5791.47\\
\bottomrule
\end{tabular}
}
\caption{Tests summary}
\label{tab:tests_summary}
\end{table}
\end{landscape}
