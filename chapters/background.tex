\chapter{Background} \label{chap:bckg}

\epigraph{
\itshape
IT IS SAID with truth that every building is constructed stone by stone, and the same may be said of knowledge, extracted and compiled by many learned men, each of whom builds upon the works of those who preceded him. What one of them does not know is known to another, and little remains truly unknown if one seeks far enough.
}{\itshape George R.R. Martin, The World of Ice \& Fire}$\:$\\
This chapter exposes the background of this thesis, and reviews the main tools we are going to employ for such purpose. The chapter, at first, introduces the \gls{hsa} (\Cref{sec:heterogeneous_architectures}), then analyzes the heterogeneous architecture we are targeting (\Cref{sec:ARM_big_LITTLE}). The two main tools we are going to use in this work are presented in \Cref{sec:odroid} and \Cref{sec:save}. Finally, \Cref{sec:back_summary} sums up the chapter and gives some hints about how this work will be developed.
\section{Heterogeneous System Architectures}\label{sec:heterogeneous_architectures}
\glspl{hsa} are becoming increasingly adopted in several scenarios \cite{hsa-fundation, amd-hsa, amd-hsa-review}; in particular, they may be remarkably useful in reaching performance efficiency and controlling energy consumption, although it means a greater system management complexity as well. For instance, it is crucial to find the best way to allocate tasks on system resources, based on task features.\\
An \gls{hsa} is a single system combining different processing units (\glspl{cpu}, \glspl{gpu} and \glspl{fpga}), which provide different solutions and trade-offs in terms of performance and power consumption. For instance, \glspl{fpga} may dynamically supply a hardware implementation of a software description, while \glspl{cpu} may execute generic tasks, and, finally, \glspl{gpu} are more convenient for parallel repetitive tasks. Since the system is conscious of underlying architecture, it can allocate a task, according to its characteristics, to the most suitable resource, and so exploit the resource nature and features. This results in a more efficient execution at both performance and energy consumption ends.
\section{ARM big.LITTLE} \label{sec:ARM_big_LITTLE}
The \gls{hsa} we focus on is proposed by ARM \cite{arm} and it is known as \textbf{big.LITTLE} technology \cite{bigLITTLE, big_little_first_whitepaper, bigLITTLE_whitepaper, benefits_of_big_little}. Such solution is an \textit{asymmetric multiprocessor system} that was introduced in 2011, and is being used in embedded systems and mobile devices, thanks to the possibility to grant power and energy benefits to the final device. ARM big.LITTLE was designed to address two fundamental requirements: 
\begin{itemize}
\item high compute capability within thermal bounds (high performance end),
\item very low power consumption (low performance end).
\end{itemize}
Following this vision, ARM big.LITTLE technology is powered by two types of processors, resulting in a heterogeneous processing architecture.
In this section, we will describe ARM big.LITTLE architecture and main features.

\subsection{Architecture} \label{subsec:architecture}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{figures/background/big_little_system}
\caption{big.LITTLE system}
\label{fig:big_little_system}
\end{figure}
ARM big.LITTLE architecture (illustrated in \Cref{fig:big_little_system}) may have different processors configurations. The one we present here is the configuration available on Samsung \cite{samsung} Exynos 5422 \gls{soc} \cite{exynos5422}, composed by the following subsystems:
\begin{itemize}
\item a cluster of ARM Cortex-A15 cores \cite{cortex-a15}, with a shared Level 2 cache,
\item a cluster of ARM Cortex-A7 cores \cite{cortex-a7}, with a shared Level 2 cache,
\item a \gls{cci}, the ARM CoreLink \gls{cci}-400 interconnect \gls{ip} \cite{cci-400},
\item a I/O coherent master,
\item a \gls{gic}, the CoreLink GIC-400 \cite{gic-400}, which dynamically distributes interrupts to all the cores.
\end{itemize}
In \Cref{subsubsec:a15_a7} and \Cref{subsubsec:cci} we will describe more in detail the processing clusters architecture and \gls{cci}.

\subsubsection{Cortex-A15 and Cortex-A7 architecture} \label{subsubsec:a15_a7}
\begin{figure}
\centering
\subbottom[Cortex-A15 pipeline\label{fig:a15_pipeline}]
{
\includegraphics[width=\textwidth]{figures/background/a15_pipeline}
}
\subbottom[Cortex-A7 pipeline
\label{fig:a7_pipeline}]
{
\includegraphics[width=0.5\textwidth]{figures/background/a7_pipeline}
}
\caption{big.LITTLE Cortex cores pipelines}
\label{fig:pipelines}
\end{figure}
As stated previously, the ARM big.LITTLE architecture we are considering is composed by two processing clusters:
\begin{itemize}
\item a \emph{big} cluster consisting of high performance cores (the ARM Cortex-A15),
\item a \emph{LITTLE} cluster consisting of low powered cores (the ARM Cortex-A7).
\end{itemize}
Since both processors support the same \gls{isa}, i.e. the ARMv7-A, they are therefore able to handle the same instructions and the same higher-level software applications. On the other hand, their internal micro-architectures are different. In this way, the big.LITTLE design is able to provide different levels of power and performance, and so satisfy the requirements previously listed.\\
\Cref{fig:pipelines} shows the pipelines of both Cortex-A15 and Cortex-A7 processors. In particular, Cortex-A15 leverages an out-of-order, triple issue processor, with a 15 to 24 stages pipeline, as displayed in \Cref{fig:a15_pipeline}. Cortex-A7 is an in-order, non-symmetric dual-issue processor, with a 8 to 10 stages pipeline, as represented in \Cref{fig:a7_pipeline}. Different pipeline length and complexity result in different power and performance levels. Indeed, on one hand, the Cortex-A15 delivers high performance, but requires a high power consumption, on the other, the Cortex-A7 is designed to be power efficient, hence its performance are lower.

\subsubsection{Cache Coherency Interface} \label{subsubsec:cci}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{figures/background/cci}
\caption{Cache Coherency Interface system}
\label{fig:cci}
\end{figure}
The idea behind the ARM big.LITTLE solution is to dynamically instantiate the right task to the right processor. Since different tasks mean different performance and power requirements, it is crucial to allocate them to the most suitable core family. Usually, most of the tasks can be properly executed by one or more Cortex-A7 cores. For instance, in a smartphone context, most of the normal telephony-related functions can be handled by the Cortex-A7 cluster. However, in case of performance hungry tasks, if Cortex-A7 cores cannot satisfy the requirement, then Cortex-A15 cores are turned on. In this way, the task is migrated from the Cortex-A7 to the Cortex-A15 cores, and so it may leverage the big cluster and respect the requirement. Tasks may be re-allocated back to the Cortex-A7 cores, when high performance are no longer needed. This implies that one or more Cortex-A15 cores may be switched off, and, as consequence, that the power consumption is reduced.\\
It is clear that one of the critical points of the ARM big.LITTLE architecture is the migration time, i.e. the time needed to migrate a task from one processing cluster to another. If the time required to switch context was too long, the system performance would be noticeable affected. For this reason, an ad-hoc interconnection bus, the \gls{cci}, was introduced to transfer data among clusters. This ensures that the task execution is not affected. Indeed, on a device at 1 GHz, the context switching is completed in less than 20,000 clock cycles \cite{big_little_first_whitepaper}.\\
\gls{cci} relies on the AMBA \gls{ace} protocol \cite{amba-ace}, which extends AMBA \gls{axi} protocol and supplies a coherent data transfer at bus level \cite{amba}. AMBA \gls{ace} protocol requires three more coherency channel, in addiction to AMBI \gls{axi} five channels. \Cref{fig:cci} illustrates how \gls{cci} system works in a coherent data read from Cortex-A7 cluster to Cortex-A15 cluster. In particular:
\begin{enumerate}
\item Cortex-A7 cluster issues a \emph{Coherent Read Request} through its own RADDR channel. \gls{cci} is in charge of taking such request to Cortex-A15 cluster ACADDR to snoop into its cache. 
\item When Cortex-A15 cluster receives the request, it checks the data availability and returns such information through CRRESP channel. If the data is really available, it is placed on the Cortex-A15 cluster CDATA channel.
\item Then, \gls{cci} transfers the data from Cortex-A15 cluster CDATA channel to Cortex-A7 cluster RDATA channel. This means a cache linefill in Cortex-A7 cluster.
\item Finally, Cortex-A7 cluster returns a \emph{Data Ack} to \gls{cci} to state the correct data transfer.
\end{enumerate}
Thanks to AMBA \gls{ace} protocol, full coherency between Cortex-A15 and Cortex-A7 cluster is ensured, without external memory transactions.

\subsection{Schedulers} \label{subsec:schedulers}
\begin{figure}
\centering
\subbottom[Cluster Migration\label{fig:cluster_migration}]
{%
\includegraphics[scale=0.6]{figures/background/cluster_migration}}
\subbottom[CPU Migration\label{fig:cpu_migration}]
{%
\includegraphics[scale=0.6]{figures/background/cpu_migration}}
\subbottom[Global Task Scheduling
\label{fig:gts}]
{%
\includegraphics[scale=0.6]{figures/background/gts}}
\caption{big.LITTLE schedulers}
\label{big_little_schedulers}
\end{figure}
The three main schedulers used on ARM big.LITTLE architecture are: \emph{Cluster Migration} (\Cref{subsubsec:cluster_migration}), \emph{\gls{cpu} Migration} (\Cref{subsubsec:cpu_migration}) and \emph{\gls{gts}} (\Cref{subsubsec:hmp}), as shown in \Cref{big_little_schedulers}.

\subsubsection{Cluster Migration} \label{subsubsec:cluster_migration}
Cluster Migration scheduler groups the cores according to their type; therefore, there are two clusters of cores: the big cluster and the LITTLE cluster. This implies that the \gls{os} can see one of two processor cluster, instead of all the cores actually available on the big.LITTLE architecture.\\
Cluster Migration scheduler allows that only one cluster can be active at the time, while the other is powered off. Hence, tasks are allocated on either big cores cluster or LITTLE cores cluster. It is clear that this approach does not scale properly, since, if a \gls{cpu} intensive task and a light one are running, both must be allocated on the same cluster, while each task should be allocated to the most suitable core, according to the task characteristics.

\subsubsection{CPU Migration} \label{subsubsec:cpu_migration}
\gls{cpu} Migration scheduler requires that the number of big cores is equal to the number of LITTLE cores. The idea is similar to Cluster Migration, but, instead of clustering cores by type (a big and a LITTLE cluster), here each big core is paired to a LITTLE core, as illustrated in \Cref{fig:cpu_migration}. In this way, the \gls{os} does not see the single cores, but the pairs as logical processing units.\\
In each pair, there is only one active core at the time, whereas the other is switched off. This means that each pair may be either a big or a LITTLE core, and, according to the workload, the most suitable core is dynamically activated by \gls{dvfs}.

\subsubsection{Heterogeneous Multi-Processing} \label{subsubsec:hmp}
ARM \gls{gts} implementation, also known as ARM big.LITTLE \gls{hmp}, does not require an equal number of big and LITTLE cores. Indeed, as displayed in \Cref{fig:gts}, the cores are no longer grouped in pairs, hence the \gls{os} task scheduler sees all the available cores and understands their different computing and power features (big or LITTLE). The scheduler analyzes the performance required by each thread, the current workload on each processor, and, thanks to statistical data and heuristics, is able to allocate the thread to the most suitable core and balance threads between big and LITTLE cores. Like \gls{cpu} Migration, the unused cores, or a whole cluster, are turned off. However, differently from \gls{cpu} Migration, the system can deploy all cores, instead of half of them. Moreover, ARM big.LITTLE \gls{hmp} can isolate intensive threads on big cores and light threads on LITTLE cores. Finally, ARM big.LITTLE \gls{hmp} targets interrupts to individual cores, while \gls{cpu} Migration migrates all the context, including interrupts, between big and LITTLE cores.\\
In order to properly migrate threads from one core to another, ARM big.LITTLE \gls{hmp} uses a tracked load metric system based on two configurable thresholds: the \emph{up migration threshold} and the \emph{down migration threshold}. For instance, if the average tracked load of a thread running on a LITTLE core surpasses the up migration threshold, then ARM big.LITTLE \gls{hmp} may decide to move such thread to a big core. On the other hand, when the average tracked load of a thread allocated on a big core falls under the down migration threshold, the thread is may be migrated to a LITTLE core. Therefore, at cluster level, ARM big.LITTLE \gls{hmp} is responsible for properly allocating and, in case, migrating thread between big and LITTLE clusters. Within clusters, standard Linux scheduler \gls{smp} balances the load across the cluster cores. Besides, \gls{smp} has been recently updated in Linux 4.3, to make its metric more precise and more representative \cite{smp_4.3}.\\
The tracked load metric system collects information according to the processor frequency. This means that ARM big.LITTLE \gls{hmp} and \gls{dvfs} mechanisms can easily cooperate without problems. Moreover, ARM big.LITTLE \gls{hmp} task migration is supported by a set of software thread affinity management techniques. In particular:

\begin{description}
\item[fork migration:] when a new thread is created, there is no tracked load history, hence it is allocated on a big core, so that it can be easily migrated to a LITTLE cores in case of a light workload thread;
\item[wake migration:] when a task moves from idle to run state, its tracked load history is analyzed and, usually, the task is assigned to the cluster it used to run on;
\item[forced migration:] In case of threads that do not sleep, or not so often, their tracked load history is periodically checked and they are migrated according to the configurable thresholds system;
\item[idle-pull migration:] when no task is allocated to a big core, load metrics of tasks running on LITTLE cluster are analyzed and, if a task exceeds the up migration threshold, it is migrated to the idle big core, otherwise it is switched off; 
\item[offload migration:] when LITTLE cores are idle or under-utilized, threads on big cores are periodically migrated downwards to exploit unused compute capacity, while they still remain eligible for up migration.
\end{description}

\section{Odroid XU3} \label{sec:odroid}
\begin{figure}
\centering
\includegraphics[scale=0.5]{figures/background/odroid_xu3}
\caption{Odroid XU3 development board}
\label{fig:odroid_xu3}
\end{figure}
The development board used in this thesis work is the \textbf{Odroid XU3} \cite{odroid}. Such board, powered by the ARM big.LITTLE technology, is a new generation of computing device, which includes powerful and energy-efficient hardware and smaller form factor.\\
The main specifications of Odroid XU3 board are listed in \Cref{tab:odroid_features}.
The Odroid XU3 board can run various distributions of Linux, including the Ubuntu \gls{os} \cite{ubuntu} and the Android \gls{os} \cite{android}. In particular, the Linux distribution we used is Lubuntu 14.04 \cite{lubuntu}.\\
The Odroid XU3 feature we are interested in the most is \gls{hmp} scheduler. Indeed, in September 2013, Samsung announced that \gls{hmp} solution \cite{yu2013power, hmp, hmp-samsung} would be implemented for their architectures \cite{hmp_announced}, starting from Exynos 5420 \cite{exynos5420}. In previous architectures, like Exynos 5410 \cite{exynos5410}, Samsung used Cluster Migration scheduler, but it was not the right choice to fully maximize the benefits of ARM big.LITTLE solution. In \Cref{chap:hmp_analysis}, we will analyzed \gls{hmp} behavior when it deals with multi-threaded \gls{cpu} intensive tasks.

\begin{table}
\centering
\scalebox{0.9}{
\begin{tabular}{cl}
\toprule
\textbf{architecture} & Samsung Exynos 5422 \gls{soc}\\
\midrule
\textbf{\glspl{cpu}} & \pbox{20cm}{ARM Cortex-A15 1.9GHz quad core\\ and ARM Cortex-A7 1.3GHz quad core} \\
\midrule
\textbf{\gls{gpu}} & ARM Mali-T628 MP6 \cite{mali}\\
\midrule
\pbox{1.1cm}{\textbf{\gls{ram}}} & \pbox{20cm}{2Gbyte LPDDR3 at 933MHz (14.9GB/s memory\\ bandwidth), \gls{pop} stacked}\\
\midrule
\textbf{storage} & eMMC5.0 HS400 Flash Storage\\
\midrule
\textbf{interfaces} & \pbox{20cm}{1 x USB 3.0 Host, 1 x USB 3.0 \gls{otg},\\ 4 x USB 2.0 Host, HDMI 1.4a and DisplayPort 1.1}\\
\midrule
\textbf{other} & integrated power consumption monitoring tool\\
\bottomrule
\end{tabular}
}
\caption{Odroid XU3 specifications}
\label{tab:odroid_features}
\end{table}
\section{EU SAVE Project} \label{sec:save}
\gls{save} is a European collaborative research project \cite{save, durelli2014save, durelli2014runtime}. Such project is funded by the Seventh Framework Programme \cite{fp7}, and it aims at developing software/hardware technologies able to efficiently leverage \glspl{hsa}.\\
The goal of \gls{save} project is to develop a system able to autonomously choose the most convenient computing resource (e.g. \gls{cpu}, \gls{gpu} or \gls{fpga}) where a task has to be executed. This is done by exploiting self-adaptivity and hardware-assisted virtualization. Such system aims at ensuring requirements of actual \gls{hpc} scenario characterized by various and flexible on-demand computing workloads, rather than static and high-performance oriented as they used to be. \gls{save} project developers decided to focus on \glspl{hsa} since they are the most suitable architecture capable of satisfying user-defined optimization goals (like performance, energy, reliability and so on). The developers' vision results in an architecture that is more dynamic and adaptable to workload features, while heads for energy consumption minimization.

\subsection{SAVE Virtual Platform} \label{subsec:save_virtual_platform}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{figures/background/simulator}
\caption{Structure of SAVE Virtual Platform}
\label{fig:simulator}
\end{figure}
The other tool we are going to employ in this thesis work is \gls{save} Virtual Platform \cite{SaveVP, save_simulator}, a \gls{hsa} simulator.\\
Since heterogeneity implies a high cost in both design and system management complexity, it is critical to define novel and innovative run-time resource management policies capable of, autonomously, distribute workloads onto convenient resources, in order to satisfy tasks \gls{sla}, like throughput, power/energy consumption. However, implementation and, especially, validation of such policies is not an easy task. For instance, it requires the availability of the real target platform for the implementation, although this may lead to a policy limited to that particular target platform. Moreover, once the run-time management policy has been implemented, it is necessary to port and test it on other different architectures. Summing up, it is clear that a fast design and prototyping tool for run-time resource management policies on \glspl{hsa} may be useful. In this way, the designers can concentrate on the policy implementation, without considering secondary issues, and so easily achieve a general and consistent validation of the policy.\\
\gls{save} Virtual Platform is a system-level simulation framework implemented in SystemC and \gls{tlm} \cite{systemC} (\Cref{fig:simulator}). Such framework is composed by a set of functional models for the \gls{hsa}. More in detail, applications are represented as task graphs, where the computational kernel nodes may have different implementations based on the available resources. On the other hand, SystemC modules model the pool of generic resources (e.g. Cortex-A15 \gls{cpu}, Cortex-A7 \gls{cpu}, Mali \gls{gpu}), and, based on cycle-accurate simulators and application execution profiling, their performance and power consumption. Thus, \gls{save} Virtual Platform allows to configure and simulate a complex homogeneous or heterogeneous system architecture and execute different workloads on the available processing units. Finally, the designer can implement and test run-time resource management policies thanks to the \emph{governor} module, which is in charge of allocating the proper computational units to the applications.

\section{Summary} \label{sec:back_summary}
In this chapter we introduced the main tools we are going to exploits for this thesis work. In particular, we presented the Odroid XU3 development board and the \gls{save} Virtual Platform. In the vision of this work, we are going to implement a workload-aware run-time resource management policy for ARM big.LITTLE architecture. Such policy is designed for multi-threaded high \gls{cpu} intensive tasks, and will take advantage of \gls{dvfs} techniques in order to both guarantee \gls{qos} and, since we are using a heterogeneous architecture, the most power-efficient configuration, where a configuration is a combination of cores (big and LITTLE) number and frequency. Moreover, we will leverage \gls{hmp} weak points in dealing with multi-threaded applications (as we will discuss in \Cref{chap:soa}), so to improve our run-time resource management policy performance, like throughput, power and energy consumption. Therefore, our policy is a combination of \gls{dvfs} techniques and workload analysis in order to properly schedule one or more tasks on the available resources. The policy will be implemented and tested on the \gls{save} Virtual Platform and then validated on the Odroid XU3 development board.