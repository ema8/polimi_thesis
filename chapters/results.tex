\chapter{Experimental Results} \label{chap:results}

\epigraph{
\itshape
Now! This is it! Now is the time to choose! Die and be free of pain, or live and fight your sorrow! Now is the time to shape your stories! Your fate is in your hands!
}{
\itshape Auron, Final Fantasy X
}$\:$\\
In this chapter, we provide the analysis we performed on our workload-aware run-time resource management policy. In particular, at first, we tested it on \gls{save} Virtual Platform (\Cref{sec:tests_simulator}), in both a single and multiple applications contexts. Then, we ported our policy to the Odroid XU3 development board and evaluated it using \gls{bs} and \gls{pcc} benchmarks (\Cref{sec:tests_odroid}). Finally, we analyzed the overhead introduced by our policy (\Cref{sec:overhead}).

\section{Tests on SAVE Virtual Platform}\label{sec:tests_simulator}
In this section, we present the tests we performed on \gls{save} Virtual Platform in order to prototype our workload-aware run-time resource management policy. Although \gls{save} Virtual Platform allows to select the underlying hardware (in our case, we chose the most similar architecture to the one mounted on Odroid XU3, i.e. 4 Samsung Exynos 5420 big cores and 4 Samsung Exynos 5420 LITTLE cores), it has some limitations. For instance, it is not possible to directly map threads on cores, but \gls{save} Virtual Platform exploits a load-balancing algorithm that, given \texttt{N} threads, distributes the workload as fair as possible. This means that we cannot completely test the efficiency of our thread mapping solution, since we can only enforce the number of threads we want to create. Another limitation is the fact that \gls{hmp} scheduler is not implemented on \gls{save} Virtual Platform, hence we cannot compare our policy with it during the prototyping phase. 

\subsection{Single Application}\label{subsec:single_app_simulator}
The application we considered for these tests is Black Scholes application since it is a well-balanced multi-threaded application. We ran several tests where we varied the throughput goal of Black Scholes to check the goodness of our policy.

\subsubsection{Power efficiency test}
In the first test we present, we set the minimum throughput goal of Black Scholes application to \texttt{2Msps}. In particular, we ran two different tests: the former test uses the complete policy, as described in \Cref{chap:implementation} and now one called \textit{power efficient policy}, the latter executes the policy without its \textit{Step 7}, now on called \textit{power inefficient policy}. In this way, we want to prove that our policy really converges to the most power efficient configuration, and satisfies the throughput goal as well.\\
In \Cref{fig:policy_throughput_simulator_goal_2}, we show the throughput reached by both our policy executions. The power inefficient policy guarantees the goal by choosing \texttt{C264} ($<1, 1400, 3, 900>$), while power efficient policy provides an higher throughput since it is set to a higher speedup configuration, i.e. \texttt{C313} ($<0, 800, 4, 1200>$). On the other hand, we can notice that the configuration chosen by the power efficient policy results to be the least power-hungry configuration (\Cref{fig:policy_power_simulator_goal_2}). Indeed, \texttt{C313} turns out the be the most power efficient configuration of the ones following \texttt{C264}. Although the choice of switching to a more power efficient configuration is based on the formulas we introduced in \Cref{chap:design}, and so the computed power values may not be accurate, it still provides a convenient and reliable ordering of configurations, differently from the speedup values ordering.\\
This result shows that our policy is able to retrieve the best configuration, in terms of power consumption, that guarantees the required throughput.

\clearpage
\begin{figure*}
\centering
\subbottom[Policy throughput \label{fig:policy_throughput_simulator_goal_2}]
{
\includegraphics[width=\textwidth]{figures/results/simulator/one_app/bs_2_thr}
}
\subbottom[Policy power consumption
\label{fig:policy_power_simulator_goal_2}]
{
\includegraphics[width=\textwidth]{figures/results/simulator/one_app/bs_2_power}
}
\caption{Single-app test on SAVE Virtual Platform}
(goal = $\texttt{2} \texttt{Msps}$)
\end{figure*}
\clearpage

\subsubsection{Other tests}
Here we presents all the other tests we ran on the \gls{save} Virtual Platform for the single application case.\\
In \Cref{fig:policy_throughput_simulator_all} we can notice that our policy always satisfies the different throughput goals of Black Scholes benchmark. \Cref{tab:policy_simulator_all} contains the tested throughput goals and the configurations the policy converges to. In terms of power consumption, \Cref{fig:policy_power_simulator_all} reports the power required by each different simulation. It is clear that the power consumption charts are not meaningful by themselves since they are not compared with other solutions. In \Cref{sec:tests_odroid}, we will compare the power consumed by our policy with the power required by \gls{hmp} scheduler.\\
These tests, among with the previous ones, demonstrate the goodness of our policy, at least during prototyping phase, when it has to deal with one single running application. In terms of throughput, the policy guarantees the \gls{qos} required by the benchmark, while, at power consumption end, these tests are not so meaningful since we only have an esteem of the real power required by the benchmark and, most important, we do not have a policy that simulate \gls{hmp} scheduler. However, power consumption tests were useful to check whether our policy can select the most power efficient configuration, among the ones capable of respecting the throughput goal, or not.

\begin{table}
\centering
\scalebox{1}{
\begin{tabular}{cc}
\toprule
\textbf{Goal} & \textbf{Convergence}\\
\midrule
\midrule
$\texttt{1}\texttt{Msps}$ & \texttt{C42} ($<0, 800, 3, 800>$) \\
\midrule
$\texttt{3}\texttt{Msps}$ & \texttt{C708} ($<2, 900, 4, 1200>$)\\
\midrule
$\texttt{4}\texttt{Msps}$ & \texttt{C981} ($<4, 1000, 4, 1000>$)\\
\midrule
$\texttt{4.5}\texttt{Msps}$ & \texttt{C1124} ($<4, 1100, 4, 1300>$)\\
\bottomrule
\end{tabular}
}
\caption{Single-app other tests on SAVE Virtual Platform summary}
\label{tab:policy_simulator_all}
\end{table}

\clearpage
\begin{figure*}
\centering
\subbottom[Policy throughput with different goals \label{fig:policy_throughput_simulator_all}]
{
\includegraphics[width=\textwidth]{figures/results/simulator/one_app/bs_all_thr}
}
\subbottom[Policy power consumption with different goals
\label{fig:policy_power_simulator_all}]
{
\includegraphics[width=\textwidth]{figures/results/simulator/one_app/bs_all_power}
}
\caption{Single-app other tests on SAVE Virtual Platform}
\end{figure*}
\clearpage

\subsection{Multiple Applications}\label{subsec:multi_app_simulator}
The applications we monitored for these tests are \gls{bs} and \gls{pcc} benchmarks. In the following tests, we first start \gls{pcc} execution, then, after \texttt{200ms} (to be sure that the policy has converged), \gls{bs} execution starts too. In this way, the policy execution can be split in three parts: \gls{pcc} execution, \gls{bs} and \gls{pcc} execution, \gls{bs} execution. Hence, the policy will converge to three different configurations according to the application set.\\
Like in single application case, we ran several tests where we varied the throughput goal of \gls{bs} and \gls{pcc} to check the goodness of our policy.

\subsubsection{Power efficiency test}
In the power efficiency test, we set the minimum throughput goal of \gls{bs} application to \texttt{1Msps}, while \gls{pcc} application goal to \texttt{10fps}. Again, we performed a test with power efficient policy and one with power inefficient policy.\\
\Cref{fig:bs_and_pcc_throughput_simulator_goal_1_10} displays the throughput curves in both cases. In the power efficient test, \gls{pcc} reaches its throughput goal, and then moves to \texttt{C11} ($<0, 800, 2, 800>$). Once \gls{bs} execution starts, the policy converges to a new configuration able to satisfy both \gls{pcc} and \gls{bs} goals, i.e. \texttt{C667} ($<2, 800, 4, 1200>$). After \gls{pcc} end, the policy converges to power efficient configuration \texttt{C42} ($<0, 800, 3, 800>$), which is the same configuration \gls{bs} converged to in single application case. On the other hand, using power inefficient policy, throughput goals are still reached; indeed, policy converges, respectively, to \texttt{C6} ($<0, 800, 1, 1200>$), \texttt{C613} ($<2, 1500, 3, 900>$) and \texttt{C32} ($<1, 900, 1, 1100>$). However, if we look at power consumption chart in \Cref{fig:bs_and_pcc_power_simulator_goal_1_10}, the power efficiency of both tests are quite different. Indeed, we can notice that power efficient policy, at first, converges to the same configuration of power inefficient policy, but then it moves to a more power efficient configuration, in all three parts of policy execution (\gls{pcc} only, \gls{bs} and \gls{pcc}, \gls{bs} only).\\
This test proves that, also in a multiple applications context, our policy is able to retrieve the best configuration, in terms of power consumption.

\begin{figure*}
\centering
\subbottom[BS and PCC throughput \label{fig:bs_and_pcc_throughput_simulator_goal_1_10}]
{
\includegraphics[width=\textwidth]{figures/results/simulator/multi_app/bs_1_pcc_10_thr}
}
\subbottom[Policy power consumption
\label{fig:bs_and_pcc_power_simulator_goal_1_10}]
{
\includegraphics[width=\textwidth]{figures/results/simulator/multi_app/bs_1_pcc_10_power}
}
\caption{Multi-apps test on SAVE Virtual Platform}
(goal = $\texttt{1}\texttt{Msps}$ and $\texttt{10}\texttt{fps}$)
\end{figure*}
\clearpage

\subsubsection{Other tests}

\begin{table}
\centering
\scalebox{1}{
\begin{tabular}{ccc}
\toprule
\textbf{Goal} & \textbf{App} & \textbf{Convergence}\\
\midrule
\midrule
$\texttt{15}\texttt{fps}$ & PCC & \texttt{C14} ($<0, 800, 2, 900>$)\\
$\texttt{1}\texttt{Msps}$ - $\texttt{15}\texttt{fps}$ & BS - PCC & \texttt{C667} ($<2, 800, 4, 1200>$)\\
$\texttt{1}\texttt{Msps}$ & BS & \texttt{C42} ($<0, 800, 3, 800>$)\\
\midrule
$\texttt{20}\texttt{fps}$ & PCC & \texttt{C42} ($<0, 800, 3, 800>$)\\
$\texttt{1}\texttt{Msps}$ - $\texttt{20}\texttt{fps}$ & BS - PCC & \texttt{C667} ($<2, 800, 4, 1200>$)\\
$\texttt{1}\texttt{Msps}$ & BS & \texttt{C42} ($<0, 800, 3, 800>$)\\
\midrule
$\texttt{20}\texttt{fps}$ & PCC & \texttt{C42} ($<0, 800, 3, 800>$)\\
$\texttt{2}\texttt{Msps}$ - $\texttt{20}\texttt{fps}$ & BS - PCC & \texttt{C1049} ($<4, 900, 4, 1300>$)\\
$\texttt{2}\texttt{Msps}$ & BS & \texttt{C667} ($<2, 800, 4, 1200>$)\\
\midrule
$\texttt{20}\texttt{fps}$ & PCC & \texttt{C42} ($<0, 800, 3, 800>$)\\
$\texttt{3}\texttt{Msps}$ - $\texttt{20}\texttt{fps}$ & BS - PCC & \texttt{C1223} ($<4, 1900, 4, 1300>$)\\
$\texttt{3}\texttt{Msps}$ & BS & \texttt{C708} ($<4, 1100, 4, 1300>$)\\
\bottomrule
\end{tabular}
}
\caption{Multi-apps other tests on SAVE Virtual Platform summary}
\label{tab:bs_and_pcc_simulator_all}
\end{table}

Here we report all the other tests we ran on the \gls{save} Virtual Platform for the multiple applications case.\\
In \Cref{fig:bs_and_pcc_throughput_simulator_all} we show that our policy always guarantees the \gls{qos} required by both \gls{bs} and \gls{pcc} applications. \Cref{tab:bs_and_pcc_simulator_all} contains the tested throughput goals and the configurations the policy converges to, in each of the three execution parts. In terms of power consumption, \Cref{fig:bs_and_pcc_power_simulator_all} reports an overestimation of the power that our policy, along with \gls{bs} and \gls{pcc} may require during its execution.\\
Like in the single application case, we demonstrate the efficiency of our policy during the simulations we performed on \gls{save} Virtual Platform. Now that both single and multiple applications cases have been tested, we can port our policy on the Odroid XU3 development board and test our solution on a real system, and, most important, compare its results with \gls{hmp} scheduler.

\clearpage
\begin{figure*}
\centering
\subbottom[BS and PCC throughput with different goals \label{fig:bs_and_pcc_throughput_simulator_all}]
{
\includegraphics[width=\textwidth]{figures/results/simulator/multi_app/all_thr}
}
\subbottom[Policy power consumption with different goals
\label{fig:bs_and_pcc_power_simulator_all}]
{
\includegraphics[width=\textwidth]{figures/results/simulator/multi_app/all_power}
}
\caption{Multi-apps other tests on SAVE Virtual Platform}
\end{figure*}
\clearpage

\section{Tests on Odroid XU3}\label{sec:tests_odroid}
After that, the policy has been tested on \gls{save} Virtual Platform in both single and multiple applications cases, we can test it on a real system. In this section, we present the tests we ran using our policy on the Odroid XU3 development board. In this way, we can overcome the limitations imposed by the \gls{save} Virtual Platform, like the direct thread mapping on cores (so far, we relied on the load-balancing algorithm available on the \gls{save} Virtual Platform). Therefore, we now can completely test each part of our workload-aware run-time resource management policy. Moreover, here it is finally possible to compare the behavior of our policy with \gls{hmp} scheduler. Indeed, in each of the tests we performed, we also ran an execution of the same benchmark using \gls{hmp} and imposing the same configuration our policy converged to. This is done because we cannot impose a throughput goal to \gls{hmp} scheduler, hence we need to execute the application using the same resources employed by our policy. In this way, performance, in terms of throughput and power consumption, of both our policy and \gls{hmp} scheduler can be properly compared. As result, we can prove whether our policy is capable of satisfying application throughput requirements and, at the same time, reduce the power consumption.\\
Finally, in these tests we have to consider that, when we compare our policy and \gls{hmp} scheduler, a power consumption comparison makes sense only if both solutions satisfy the application goal, otherwise, even though it may still be interesting, such comparison turns out to be less meaningful. The primary goal of our policy is the fulfillment of the application throughput goal. Once such requirement is respected, the policy can focus on its secondary goal; i.e. power efficiency.

\subsection{Single Application}\label{subsec:single_app_board}
The application we considered for the tests on Odroid XU3 development board is, again, Black Scholes application, for the reasons stated before. For these tests, we set the number of Black Scholes kernel executions to 500, while each kernel computes 1000000 options. Like in the tests on \gls{save} Virtual Platform, we performed several tests varying the throughput goal of Black Scholes benchmark.

\subsubsection{Full speed test} \label{subsubsec:fullspeed}
In this test, we compare the maximum throughput that can be provided by our policy and \gls{hmp} scheduler at full speed, i.e. in \texttt{C1223} ($<4, 1900, 4, 1300>$). We set a sufficiently high goal in order to let the policy converge to \texttt{C1223}, while we directly enforced such configuration, using \texttt{taskset} command, when we used \gls{hmp} scheduler.\\
In \Cref{fig:policy_throughput_board_top}, it is clear that our policy is definitely more efficient than \gls{hmp} (in terms of throughput), since, thanks to thread mapping, our policy is able of reaching an average throughput of \texttt{5Msps}. On the other hand, \gls{hmp} scheduler throughput is slightly smaller than \texttt{4.5Msps}. Therefore, our policy can satisfy throughput goals that \gls{hmp} scheduler cannot, due to a not completely efficient load distribution. Finally, our policy takes \texttt{10s} to converge to \texttt{C1223}.\\
In terms of power consumption, \Cref{fig:policy_power_board_top} demonstrates that the results reported by \gls{save} Virtual Platform were not correct; indeed, those power values were overestimating the power consumption of policy. On Odroid XU3, we can truly measure the power consumed by both solutions. As result, not only the power required by our policy is smaller than the one estimated by \gls{save} Virtual Platform, but it also turns out to be more power efficient than \gls{hmp} scheduler.\\
Finally, we compared our policy and \gls{hmp} with a \textit{throughput/power} (\texttt{sps/watt}) metric. Differently from throughput/energy (\texttt{sps/joule}) metric, this one is not execution depended, i.e. it does not take into consideration execution time. \Cref{fig:policy_throughput_power_board_goal_top} proves that our policy is more efficient than \gls{hmp} also under this metric. \gls{hmp} execution in this test may be considered identical to a generic \gls{hmp} execution of \gls{bs} application; indeed, also in the latter case, \gls{hmp} spreads the workload among all the available resources. Hence, we will use this \gls{hmp} throughput-power ratio value as a criterion for comparison in the next tests. In this way, we compare the results of our policy with an usual \gls{hmp} execution.\\
This test demonstrated that our policy results to perform better than \gls{hmp} scheduler at both throughput and power consumption ends, thanks to an efficient workload distribution among available resources.

\clearpage
\begin{figure*}
\centering
\subbottom[Full speed policy and HMP throughput \label{fig:policy_throughput_board_top}]
{
\includegraphics[width=\textwidth]{figures/results/board/one_app/bs_top_thr}
}
\subbottom[Full speed policy and HMP power consumption
\label{fig:policy_power_board_top}]
{
\includegraphics[width=\textwidth]{figures/results/board/one_app/bs_top_power}
}
\caption{Single-app full speed test on Odroid XU3}
\end{figure*}
\clearpage

\begin{figure*}
\centering
\contsubbottom[Full speed policy and HMP throughput-power ratio
\label{fig:policy_throughput_power_board_goal_top}]
{
\includegraphics[width=\textwidth]{figures/results/board/one_app/bs_top_throughput_power}
}
\contcaption{Single-app full speed test on Odroid XU3}
\end{figure*}

\subsubsection{Power efficiency test}
Like we did for the tests on \gls{save} Virtual Platform, we first want to check whether our policy finds the most power efficient configuration, once the throughput goal is satisfied.
The minimum throughput goal of Black Scholes application is set to \texttt{2Msps}, and we ran two different tests, one using the power efficient policy, the other using the power inefficient policy. Then, for each test, we extracted the configurations the policy converged to and executed Black Scholes with \gls{hmp} scheduler enforcing such configurations.\\
In \Cref{fig:policy_throughput_board_goal_2}, we report the throughput reached by both policies execution and \gls{hmp} scheduler. The power efficient policy converges to \texttt{C516} ($<1, 900, 4, 1200>$), which is a Critical Configuration for \gls{hmp}, while the power inefficient policy converges to \texttt{C496} ($<3, 900, 2, 1200>$). Both policies guarantee the \gls{qos} required by Black Scholes, whereas neither of \gls{hmp} executions are capable of satisfy such goal. In particular, \gls{hmp} execution in \texttt{C516} is the one that has the lowest throughput (almost \texttt{0.4Msps}). This result is due to the fact that \texttt{C516} is a Critical Configuration, hence \gls{hmp} does not spread the workload properly, but executes all the computation on the big core only. \gls{hmp} execution in \texttt{C496} has certainly better throughput performance, but it still cannot respect Black Scholes goal. This means that our thread mapping solution effectively efficient, since it allows our policy to reach throughput values higher than \gls{hmp} scheduler.\\
In terms of power consumption, \gls{hmp} at \texttt{C516} turns out to be the most power efficient execution. Actually, this is a consequence of \gls{hmp} behavior in Critical Configurations, because \gls{hmp} employs only the big core, while the LITTLE cores are in idle state. Hence, it cannot be considered as an effective power efficient solution, since it does not satisfy the throughput goal either.
Excluding \gls{hmp} at \texttt{C516}, the power efficient policy results to be the less power-hungry solution. On the other hand, after an initial transient, the power required by our power inefficient policy and \gls{hmp} at \texttt{C496} is very similar, our policy consumes slightly more power.\\
\Cref{fig:policy_throughput_power_board_goal_2} compares our policy and \gls{hmp} using throughput/power metric. Not only both policy executions result to be more efficient than the respective \gls{hmp} executions, but they also definitely surpass full speed \gls{hmp}.\\
Finally, while power inefficient policy takes \texttt{9s} (3\% of whole execution time) to converge, power efficient one requires \texttt{19s} (7\%), since it has to find a power efficient configuration and test whether it satisfies the throughput goal.


\begin{figure*}
\centering
\subbottom[Policy and HMP throughput \label{fig:policy_throughput_board_goal_2}]
{
\includegraphics[width=\textwidth]{figures/results/board/one_app/bs_2_thr}
}
\subbottom[Policy and HMP power consumption
\label{fig:policy_power_board_goal_2}]
{
\includegraphics[width=\textwidth]{figures/results/board/one_app/bs_2_power}
}
\caption{Single-app test on Odroid XU3}
(goal = $\texttt{2}\texttt{Msps}$)
\end{figure*}
\clearpage

\begin{figure*}
\centering
\contsubbottom[Policy and HMP throughput-power ratio
\label{fig:policy_throughput_power_board_goal_2}]
{
\includegraphics[width=\textwidth]{figures/results/board/one_app/bs_2_throughput_power}
}
\contcaption{Single-app test on Odroid XU3}
(goal = $\texttt{2}\texttt{Msps}$)
\end{figure*}
%\clearpage

\subsubsection{Other tests}
Here we presents all the other tests we ran on Odroid XU3 development board for the single application case.\\
\Cref{fig:policy_throughput_board_all} shows that our policy always satisfies the throughput goal of Black Scholes on the four tested cases (\Cref{tab:policy_board_all} contains the tested throughput goals, the configurations the policy converges to, and the convergence times). On the other hand, \gls{hmp} scheduler is able to respect the goal only in one configuration (\texttt{C106}), where the system is actually used as a homogeneous architecture, since no big cores are employed. In all the other configurations, \gls{hmp} does not guarantee the minimum \gls{qos}.\\
\Cref{fig:policy_power_board_all} reports the chart power consumption charts for each test. In \texttt{C106} and \texttt{C967}, after an initial transient, the power required by our policy and \gls{hmp} is almost identical, while, in \texttt{C1192} and \texttt{C1222}, our policy consumes definitely less power than \gls{hmp} scheduler.\\
\Cref{fig:policy_throughput_power_board_goal_all} shows that, in terms of throughput-power ratio, our policy is as efficient as \gls{hmp} in \texttt{1Msps} and \texttt{3Msps} test cases, whereas it is more efficient than \gls{hmp} in the other two cases. Moreover, each execution of our policy is more throughput-power efficient than full speed \gls{hmp}.\\
In terms of convergence time, in the worst case, our policy requires \texttt{27s}, i.e. 16\% of \gls{bs} execution time, while, in the best case, it takes 3\%.\\
These tests, like the previous ones, confirm the tests we performed on \gls{save} Virtual Platform and the quality of our policy, in a single application case. Most important, these tests demonstrate the goodness of our thread mapping solution, which is capable of increasing performance (at throughput and power consumption ends) since it distributes the workload among the cores in more efficiently than \gls{hmp} scheduler. In this way, configurations that would not guarantee the minimum \gls{qos} using \gls{hmp}, may be employed to both respect the throughput goal and reduce power consumption. Finally, each of our policy executions resulted to be more throughput-power efficient than full speed \gls{hmp} execution, which, as we stated, it may be considered as a regular and usual \gls{hmp} execution in a multi-threaded context.

\begin{table}
\centering
\scalebox{1}{
\begin{tabular}{ccc}
\toprule
\textbf{Goal} & \textbf{Convergence} & \textbf{Time (\texttt{s})}\\
\midrule
\midrule
$\texttt{1}\texttt{Msps}$ & \texttt{C106} ($<0, 800, 4, 800>$) & 12 (3\%)\\
\midrule
$\texttt{3}\texttt{Msps}$ & \texttt{C967} ($<4, 900, 4, 1100>$) & 27 (16\%)\\
\midrule
$\texttt{4}\texttt{Msps}$ & \texttt{C1192} ($<4, 1400, 4, 1300>$) & 19 (15\%)\\
\midrule
$\texttt{4.5}\texttt{Msps}$ & \texttt{C1222} ($<4, 1900, 4, 1200>$) & 5 (5\%)\\
\bottomrule
\end{tabular}
}
\caption{Single-app other tests on Odroid XU3 summary}
\label{tab:policy_board_all}
\end{table}

\begin{figure*}
\centering
\subbottom[Policy and HMP throughput with different goals \label{fig:policy_throughput_board_all}]
{
\includegraphics[width=\textwidth]{figures/results/board/one_app/bs_all_thr}
}

\subbottom[Policy and HMP power consumption with different goals
\label{fig:policy_power_board_all}]
{
\includegraphics[width=\textwidth]{figures/results/board/one_app/bs_all_power}
}
\caption{Single-app other tests on Odroid XU3 summary}
\end{figure*}
\clearpage

\begin{figure*}
\centering
\contsubbottom[Policy and HMP throughput-power ratio with different goals
\label{fig:policy_throughput_power_board_goal_all}]
{
\includegraphics[width=\textwidth]{figures/results/board/one_app/bs_all_throughput_power}
}
\contcaption{Single-app other tests on Odroid XU3}
\end{figure*}
%\clearpage

\subsection{Multiple Applications}\label{subsec:multi_app_board}
Like for the multiple applications tests on \gls{save} Virtual Platform, we monitored \gls{bs} and \gls{pcc} benchmarks. Both the considered applications are multi-threaded, but \gls{pcc} is not well-balanced. In fact, \gls{pcc} benchmark on \gls{save} Virtual Platform was designed to be more balanced and stable than its actual implementation. Therefore, our policy may take more time to converge, in particular when a power efficient configuration has to be found; indeed, the fact that our speedup formula is not 100\% accurate, along with \gls{pcc} features, may force our policy to try different power efficient configuration before retrieving the right one, in terms of both throughput and power efficiency.\\
In the following tests, we first start \gls{pcc} execution, then, after \gls{pcc} convergence, \gls{bs} execution starts too. Again, the policy execution can be split in three parts: \gls{pcc} execution, \gls{bs} and \gls{pcc} execution, \gls{bs} execution.

\subsubsection{Power efficiency test}
Here we present the power efficiency test for multiple applications case. \gls{bs} application goal is \texttt{1Msps}, while \gls{pcc} goal is \texttt{20fps}.\\
\Cref{fig:bs_pcc_throughput_board_goal_1_20} shows the throughput curves in both cases. In the power efficient test, \gls{pcc} reaches its throughput goal, and then, after trying different configuration, it eventually converges to \texttt{C442} ($<1, 900, 4, 1100>$). Once \gls{bs} execution starts, the policy converges to an initial configuration able to satisfy both both, and then looks for a power efficient one. The policy converges to \texttt{C825} ($<3, 800, 4, 1200>$). Finally, after \gls{pcc} end, \gls{bs} easily converges to its power efficient configuration, i.e. \texttt{C106} ($<0, 800, 4, 800>$). On the contrary, the power inefficient policy converges after few iterations to a configuration able to guarantee \gls{pcc} \gls{qos}, i.e. \texttt{C300} ($<3, 1000, 1, 900>$); then, once \gls{bs} starts its execution, the policy converges again quite quickly to a new configuration capable of satisfying \gls{pcc} and \gls{bs} goals \texttt{C811} ($<3, 1800, 1, 900>$). Finally, when there is only \gls{bs} running, the policy converges to \texttt{C78} ($<2, 800, 1, 900>$).\\
In terms of power consumption, as reported in \Cref{fig:bs_pcc_power_board_goal_1_20}, power efficient policy, at first, has a behavior similar to power inefficient one, but then it moves to power efficient configurations, although the policy does not converge immediately to a configuration able to satisfy both power efficiency and \gls{qos}, as a consequence of \gls{pcc} unbalanced behavior.\\
\gls{pcc} convergence time and, in particular, its percentage with respect to \gls{pcc} execution time only may not be as interesting as the other values since it depends on the time instant we decided to start \gls{bs} execution. Power efficient policy takes \texttt{77s} to converge to \texttt{C442} (63\% of \gls{pcc} execution time). After \gls{bs} starts its execution, the policy converges in \texttt{105s} (73\% of \gls{bs} and \gls{pcc} execution time together). Finally, after \gls{pcc} end, the policy converges in \texttt{23s} (7\% of \gls{bs} execution time). On the other hand, power inefficient policy does not look for a power efficient configuration, hence, when only \gls{pcc} is running, it converges in \texttt{13s} (18\%). Then, as \gls{bs} begins, the policy converges again in \texttt{15s} (12\%). Finally, the policy with only \gls{bs} running converges in \texttt{13s} (4\%).\\
This test proves that, also in a multiple applications context, our policy is able to converge to a configuration that both guarantees the \gls{qos} and is power efficient.

\begin{figure*}
\centering
\subbottom[BS and PCC throughput \label{fig:bs_pcc_throughput_board_goal_1_20}]
{
\includegraphics[width=\textwidth]{figures/results/board/multi_app/bs_1_pcc_20_thr}
}
\subbottom[Policy power consumption
\label{fig:bs_pcc_power_board_goal_1_20}]
{
\includegraphics[width=\textwidth]{figures/results/board/multi_app/bs_1_pcc_20_power}
}
\caption{Multi-apps test on Odroid XU3}
(goal = $\texttt{1}\texttt{Msps}$ and $\texttt{20}\texttt{fps}$)
\end{figure*}
\clearpage

\subsubsection{Other tests}
Here we presents all the other tests we ran on Odroid XU3 development board for the multiple applications case.\\
\Cref{fig:bs_and_pcc_throughput_board_all} shows that our policy always satisfies the throughput goal of \gls{bs} and \gls{pcc} on the four tested cases, although it does not converge to a power efficient configuration as fast as it did with only \gls{bs} benchmark (\Cref{tab:bs_and_pcc_board_all} contains the tested throughput goals, the configurations the policy converges to, and the convergence times)\\
\Cref{fig:bs_and_pcc_power_board_all} reports the chart power consumption charts for each test. In all the cases, we can notice that, after an initial transient where the policy converges to a suitable configuration, the power consumption decreases since the possible power efficient configurations are tested in order to find one able to also guarantee \gls{qos} of \gls{pcc} and \gls{bs}.\\
Finally, in terms of convergence times, when \gls{pcc} is running, our policy always takes more time than the time needed to converge when only \gls{bs} is running. As stated previously, this is a consequence of \gls{pcc} unbalanced nature.

\begin{table}
\centering
\scalebox{0.9}{
\begin{tabular}{cccc}
\toprule
\textbf{Goal} & \textbf{App} & \textbf{Convergence} & \textbf{Time}\\
\midrule
\midrule
$\texttt{10}\texttt{fps}$ & PCC & \texttt{C20} ($<1, 800, 1, 1000>$) & \texttt{86s} (67\%)\\
$\texttt{1}\texttt{Msps}$ - $\texttt{10}\texttt{fps}$ & BS - PCC & \texttt{C727} ($<2, 1100, 4, 1100>$) & \texttt{136s} (67\%)\\
$\texttt{1}\texttt{Msps}$ & BS & \texttt{C106} ($<0, 800, 4, 800>$) & \texttt{22s} (9\%)\\
\midrule
$\texttt{15}\texttt{fps}$ & PCC & \texttt{C286} ($<1, 800, 4, 900>$) & \texttt{65s} (66\%)\\
$\texttt{1}\texttt{Msps}$ - $\texttt{15}\texttt{fps}$ & BS - PCC & \texttt{C727} ($<2, 1100, 4, 1100>$) & \texttt{127s} (67\%)\\
$\texttt{1}\texttt{Msps}$ & BS & \texttt{C106} ($<0, 800, 4, 800>$) & \texttt{18s} (7\%)\\
\midrule
$\texttt{20}\texttt{fps}$ & PCC & \texttt{C442} ($<1, 900, 4, 1100>$) & \texttt{72s} (71\%)\\
$\texttt{1.5}\texttt{Msps}$ - $\texttt{20}\texttt{fps}$ & BS - PCC & \texttt{C1066} ($<4, 1200, 3, 1300>$) & \texttt{48s} (45\%)\\
$\texttt{1.5}\texttt{Msps}$ & BS & \texttt{C313} ($<0, 800, 4, 1200>$) & \texttt{12s} (6\%)\\
\midrule
$\texttt{10}\texttt{fps}$ & PCC & \texttt{C20} ($<1, 800, 1, 1000>$) & \texttt{86s} (70\%)\\
$\texttt{2}\texttt{Msps}$ - $\texttt{10}\texttt{fps}$ & BS - PCC & \texttt{C1213} ($<4, 1900, 3, 1200>$) & \texttt{31s} (29\%)\\
$\texttt{2}\texttt{Msps}$ & BS & \texttt{C516} ($<1, 900, 4, 1200>$) & \texttt{30s} (20\%)\\
\bottomrule
\end{tabular}
}
\caption{Multi-apps other tests on Odroid XU3 summary}
\label{tab:bs_and_pcc_board_all}
\end{table}
\clearpage

\begin{figure*}
\centering
\subbottom[BS and PCC throughput with different goals
\label{fig:bs_and_pcc_throughput_board_all}]
{
\includegraphics[width=\textwidth]{figures/results/board/multi_app/all_thr}
}
\subbottom[Policy and HMP power consumption with different goals
\label{fig:bs_and_pcc_power_board_all}]
{
\includegraphics[width=\textwidth]{figures/results/board/multi_app/all_power}
}
\caption{Multi-apps other tests on Odroid XU3}
\end{figure*}
\clearpage

\section{Overhead Analysis}\label{sec:overhead}
So far, we have reported the tests performed on our policy on both \gls{save} Virtual Platform and Odroid XU3 development board. In this section, we analyze the overhead introduced by our policy. More specifically, we compare the performance of full speed \gls{hmp} standalone execution, already reported in \Cref{subsubsec:fullspeed}, with \gls{hmp} on policy execution. To achieve such goal, we modified our policy in order to remove task allocation and allow \gls{hmp} to distribute the workload. We ran \gls{bs} benchmark with a sufficient high throughput goal, so that the policy quickly converges to \texttt{C1223}.\\
\Cref{fig:overhead_thr} displays \gls{hmp} standalone and \gls{hmp} on policy throughput. We can notice that, while \gls{hmp} standalone throughput results to be quite stable, \gls{hmp} on policy throughput is slightly unstable, due to the interaction with the policy. \gls{hmp} standalone average throughput is \texttt{4.41Msps}, whereas \gls{hmp} on policy average throughput is \texttt{4.26Msps}.\\
\Cref{fig:overhead_power} reports the power consumption curves. On average, \gls{hmp} standalone consumes \texttt{6.09W}, while \gls{hmp} on policy \texttt{6.12W}.\\
Finally, we compared \gls{hmp} standalone with \gls{hmp} on policy in terms of throughput-power ratio metric, as showed in \Cref{fig:overhead_thr_power}. As result, \gls{hmp} standalone throughput-power ratio is higher than \gls{hmp} on policy (\texttt{0.72Msps/W} and \texttt{0.69Msps/W}, respectively).\\
As we expected, our policy introduces an overhead in terms of throughput and power consumption. The policy overhead results in a 3.5\% throughput loss, while, in terms of power consumption, the overhead is 0.5\%. Therefore, the power consumption overhead is negligible, whereas the throughput loss is not so relevant but it does not affect our policy execution since, as showed in
\Cref{subsubsec:fullspeed}, the task allocation solution is able to provide higher throughput performance than \gls{hmp} scheduler.

\begin{figure*}
\centering
\subbottom[Throughput overhead \label{fig:overhead_thr}]
{
\includegraphics[width=0.8\textwidth]{figures/results/overhead/overhead_thr}
}
\subbottom[Power consumption overhead
\label{fig:overhead_power}]
{
\includegraphics[width=0.8\textwidth]{figures/results/overhead/overhead_power}
}
\subbottom[Throughput-power ratio overhead
\label{fig:overhead_thr_power}]
{
\includegraphics[width=0.8\textwidth]{figures/results/overhead/overhead_throughput_power}
}
\caption{Policy overhead}
\end{figure*}