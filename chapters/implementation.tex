\chapter{Implementation} \label{chap:implementation}

\epigraph{
\itshape
In quel labirinto il tempo pass\`o. Sotto gli strati di neve le due pianticelle crebbero pur piegandosi e aggrovigliandosi. E un giorno gli alberi cominciarono a estendere i loro rami fuori dal chiuso del giardino dando frutti misteriosi.
}{\textit{Kentaro Miura, Berserk}\\
translated by Gianluca Bevere}$\:$\\
In this chapter we illustrate the implementation details of the workload-aware run-time resource management policy we propose in this work. As stated in the previous chapters, our policy aims at guaranteeing the \gls{qos} of the application set (at throughput end), and, at the same time, reducing their power consumption by enforcing the most convenient configuration.\\
In \Cref{sec:mapping_ratio} we show how we computed the mapping ratios for all the different combinations of big and LITTLE cores frequencies. In \Cref{sec:implementation}, we explain, step by step, how the policy works.

\section{Mapping Ratio}\label{sec:mapping_ratio}
In \Cref{chap:soa}, we proved that a proper allocation of threads on the available resources may produce improvements in terms of the application performance. The allocation was done according to the mapping ratio we found by looking at big and LITTLE cores throughput at different frequencies. In the tests we performed, we approximated such mapping ratio to 2, hence, for each thread we mapped on a LITTLE core, we mapped 2 threads on a big core. However, this approximation holds only if big frequency is set to \texttt{1900MHz} and LITTLE frequency to \texttt{1300MHz}, and, most important, it is application dependent. Therefore, we need to generalize this method in order to address all combinations of big and LITTLE cores frequency.
We define the mapping ratio as the ratio between big and LITTLE cores throughput at their given frequencies. Since this ratio is a real number, it is necessary to approximate it to a rational number. Thus, we can consider the numerator as the number of threads to allocate on each big core and the denominator as the number of threads to allocate on each LITTLE core, as the following formula suggests: 
\\
\begin{align*}
approximate(p_{ratio}) = \frac{numerator}{denominator} = \frac{threads_B}{threads_L}
\end{align*}
\\
When the policy starts or the application set changes, it would be necessary to compute the mapping ratio for each combination of big and LITTLE cores frequencies. However, this may be a long and intensive task, and, besides, it may steal time to the policy itself. A convenient solution is to choose a reference frequency for both big and LITTLE cores, compute a reference mapping ratio $\bar{p}_{ratio}$, and then obtain all the other mapping ratios from the reference one. Hence, we acquire $\bar{p}_{ratio}$ in this way:
\\
\begin{align*}
\bar{p}_{ratio} = \frac{\overline{throughput}_B}{\overline{throughput}_L}
\end{align*}
\\
Then, a generic mapping ratio $p_{ratio}$ may be compute as follows:
\\
\begin{align*}
p_{ratio} = \frac{throughput_B}{throughput_L} = \dfrac{f_B}{f_L} \cdot \dfrac{\bar{f}_L}{\bar{f}_B} \cdot \overline{p}_{ratio}
\end{align*}
\\
This formula holds since, in first approximation, throughput scales with frequency, as we showed in \Cref{chap:design}.
Finally, if the reference frequency for big and LITTLE cores is the same, the previous formula may be simplified like this:
\\
\begin{align*}
p_{ratio} = \frac{f_B}{f_L} \cdot \bar{p}_{ratio}
\end{align*}
\\
This procedure allows us to steal just a small time to the policy execution, since we just need to acquire one mapping ratio, and then compute the required mapping ratio when we find the new configuration to be actuated.\\
As a test, we both measured and computed the mapping ratios for each combination of frequencies. The configurations used to compute the reference mapping ratio are: $<4, 800, 0, 800>$ and $<0, 800, 4, 800>$, while the cores employed for the measurements are always 4 (for both big and LITTLE clusters). In \Cref{fig:mapping_ratios}, we can see that the difference between the measured and the computed values is not meaningful (the average mean difference is -0.003, while the standard deviation of the difference is 0.009). This shows the goodness of our procedure. Finally, if we use a different number of big or LITTLE cores for the measurements, the difference between the measured and computed mapping ratios is still negligible.\\
The reference mapping ratio $\bar{p}_{ratio}$ is generated in Step 2 of the policy execution (\Cref{subsec:mapping_ratio_generation}), while, in Step 5, it is  exploited to compute the mapping ratio of the configuration to be actuated (\Cref{subsec:configuration_actuation}).

\begin{figure}
\centering
\includegraphics[width=\textwidth]{figures/implementation/mapping_ratios}
\caption{Computed and measured mapping ratios}
\label{fig:mapping_ratios}
\end{figure}

\section{Implementation} \label{sec:implementation}
In this section, we present of our policy design. The policy is implemented in C++ programming language on both \gls{save} Virtual Platform and Odroid XU3 development board.\\
The policy execution Steps that were briefly introduced in \Cref{subsec:policy_heart} are now being explained in detail by the following sections. 
As described in \Cref{fig:policy_steps}, the policy steps can be summarized as follows:
\begin{enumerate}[label = Step \arabic*), leftmargin=3.2\parindent]
\item configuration table building (\Cref{subsec:configuration_table}),
\item mapping ratio generation (\Cref{subsec:mapping_ratio_generation}),
\item table baseline update (\Cref{subsec:table_baseline_update}),
\item configuration retrieval (\Cref{subsec:configuration_retrieval}),
\item power-efficient configuration retrieval (\Cref{subsec:power_efficient_configuration_retrieval}),
\item configuration actuation (\Cref{subsec:configuration_actuation}),
\item configuration reset (\Cref{subsec:configuration_reset}).
\end{enumerate}

\subsection{Configuration Table}\label{subsec:configuration_table}

For our policy to work it is necessary to know the basic resources available on the underlying architecture. Hence, we first build a map that stores the data regarding the operating frequencies and the number of cores for each available family of computing resource in the system. This guarantees the scalability of our solution as the number and family of cores change. Such information are available in the \gls{save} Virtual Platform environment, and can be collected as well on a real system by analyzing for example \textit{proc/cpuinfo} and the output of \textit{cpufreq-info} command \cite{cpufreq-info}.\\
The content of this processor map is necessary to build the configuration table, i.e. a table containing all the possible combination of number of cores (big and LITTLE) and their frequencies. As we showed in \Cref{subsec:configuration_definition}, the number of different configurations for the Exynos 5422 is 1224. As explained in \Cref{chap:design}, each configuration is associated with a power consumption value and an estimated throughput with respect to the slowest single processor execution. Such values are computed using the formulas showed in \Cref{sec:proposed_solution}.\\
Once the resources configuration table has been built, configurations are sorted by speedup, and a first configuration has to be set for the system.
We allow to configure the starting configuration point since, depending on the starting configuration the policy ends up in having different behaviors.
As an example \Cref{fig:starting_configurations} illustrates how the policy behaves picking different starting configurations in a test using \gls{pcc} benchmark.
Configurations are ordered by expected speedup, where \texttt{C0} is the minimum (i.e. 1 LITTLE core at \texttt{800MHz}) while \texttt{C1223} corresponds to the most powerful configuration (i.e. 4 big cores working at \texttt{1900MHz} and 4 LITTLE cores working at \texttt{1300MHz}).
As we can see from the figure, \texttt{C0} converges slowly and this happens since it takes time for the policy to have a stable performance measurement from the application.
A performance measurement is stable after few iterations of the application loop and, obviously, running with low performance cores increments the time needed to perform this number of iterations.
On the opposite, using \texttt{C1223} forces the system to quickly change configuration if needed and this choice allows for a faster convergence than before; however this solution causes a peak in power consumption for the first iterations, even though this choice guarantees to satisfy performance requirements from the start.
Finally other solutions are possible picking any configuration in the middle, basically the most the starting configuration is near the final one the faster the algorithm converges.
As a rule of thumb we suggest to set either \texttt{C1223}, if paying the power overhead is not a problem, or \texttt{C612} (which is the middle configuration) because then the search space is reduced by an half.
In general, since the policy is effective when the system is not overload (i.e. it does not have to work at full power) picking \texttt{C612} will help on average to have a faster convergence time.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{figures/implementation/starting_configurations}
\caption{Experiments with different starting configurations.}
\label{fig:starting_configurations}
\end{figure}

\subsection{Mapping Ratio Generation}\label{subsec:mapping_ratio_generation}
The second step of the policy is the generation of the reference mapping ratio $\bar{p}_{ratio}$. During this profiling phase, we allocate one thread on each core. As stated previously, the reference configurations are $<0, 800, 4, 800>$ and $<4, 800, 0, 800>$.
At first, we set the former configuration, and, before extracting the throughput, we wait the slowest application (i.e. the slowest one to produce Heartbeats) to go full speed. This is done to allow the application (or the slowest one when there are more applications) to converge in this configuration.
At that point, we can retrieve the throughput of each application. Then, we set the latter configuration and repeat the procedure. Once we have throughput at both $<0, 800, 4, 800>$ and $<4, 800, 0, 800>$ configuration, we can compute the reference mapping ratio $\bar{p}_{ratio}$ for each application. This value will be used in the following configuration changes to spread properly the threads among the cores.\\
Now that we have both the configuration table and the reference mapping ratio $\bar{p}_{ratio}$ for each application, the very policy can start.

\subsection{Table Baseline Update}\label{subsec:table_baseline_update}
The starting configuration we used is \texttt{C612}. Every \texttt{n} millisec the policy observes the throughput of every application (Heartbeats actually) and, every \texttt{k} measurements, the policy decides whether the system configuration has to be changed or not. The sampling period of the policy is fixed, e.g. \texttt{200ms}, while the throughput stability is application dependent.\\
The configuration table construction described before assumes as baseline, for the speedup estimation, the performance of a single core of the slowest family at the lowest operating frequency (i.e. the baseline is configuration $<0, 800, 1, 800>$). Starting from this, every time the resources configuration changes, the values of the table need to be updated. 
Based on the last chosen configuration, the policy knows what was the last estimated speedup for that particular configuration.
We then divide every estimated speedup by the last chosen one.
This means that the table now contains the values of the estimated speedups with respect to the last chosen configuration. 
We have hence set a new baseline.

\subsection{Configuration Retrieval}\label{subsec:configuration_retrieval}
As specified, the goal of the whole policy is to meet much performance goals as possible. \Cref{alg:presudocode} reports the pseudocode for the selection of the new configuration to enforce.
Given that each running application $a$ performs differently and that it has a different goal, we have adopted the following strategy to meet the policy goal. 
For each application, we compute the required speedup $S_a$ as the ratio between the current performance of the considered application $T_a$ (obtained through online monitoring) and the declared minimum throughput requirement $G_a$, i.e. $S_a = G_a / T_a$. The speedup $S_a$ is a simple way to understand, overall, how distant an application is from its performance requirement. 
The $S_a$ computation can have three possible outcomes, i.e. it can be greater, equal, or less than 1.
A $S_a$ greater than 1 means that the application has not reached its goal yet, hence a speedup $S_a$ is required. 
A $S_a$ equal to 1 is the ideal condition we want to achieve, meaning that the application is performing exactly the way it should. 
A $S_a$ less than 1 shows that the application is running faster than its declared goal, in this case we can switch to a slower configuration to be more power efficient. 
Given that our main objective function is to maximize the number of applications that reach their goals, we choose to enforce the overall maximum required speedup whenever there has been at least a $S_a$ greater than 1; we stick with the current configuration if all the $S_a$ are 1; or we try to enforce the minimum slowdown required in case all the computed $S_a$ are less than 1.\\
After a given speedup $S_a$ (slowdown if $S_a<1$) has been requested, the policy scans through the configuration table to find the configuration that can guarantee that speedup.
Once we have the speedup (slowdown) required, we start looking for the closest speedup value in the configuration table.
Since it would be useless to scan the whole table every time, we use two indexes ($c_{Low}$ and $c_{High}$) to indicate the range of configurations we have to check. Before retrieving the new configuration, we update one of those indexes according to the speedup (slowdown) value we have just computed. In particular, if we have a speedup value greater than 1, we set the $c_{Low}$ index to the index of the last configuration. This is done because, if we now need a speedup, it means that the last configuration was too slow, and so all the configurations preceding that one, since they are sorted by speedup. On the other hand, if we have a slowdown, we set the $c_{High}$ index to the index of the last configuration. As consequence, all the configurations following the last one are, for the moment, ignored because are too fast. 
In this way, the search has, on average, a logarithmic time complexity (i.e. it is $\mathcal{O}(log(n))$); hence, the policy should converge in, more or less, ten iterations. Finally, we now can retrieve the new configuration to be actuated.

\begin{algorithm}[t]
 \KwData{$c_{old}$, $c_{Low}$, $c_{High}, A$}
 \KwResult{$c_{new}$}
 $T \leftarrow acquireApplicationsPerformance()$\;\label{line:getperf}
 $G \leftarrow acquireApplicationsGoal()$\;\label{line:getgoal}
 \ForAll{$a \in A$}{\label{line:speedupStart}
	$S_a = G_a / T_a$\; 
 }\label{line:speedupEnd}

 \eIf{$max(S) \geq 1$}{\label{line:updateStart}
 	$c_{Low} = c_t$\;
 }{
 	$c_{High} = c_t$\; 
 }\label{line:updateEnd}
 
$rebaseSpeedups(c_{old})$\;
 
 \ForAll{$c \in (c_{Low}, c_{High}]$}{\label{line:getConfStart}
	\If{$speedup(c) > max(S)$}{
		$c_{new} = c$\;
		$break$\;
	}
 } \label{line:getConfEnd}
\caption{Pseudocode for the selection of next configuration.\label{alg:presudocode}}
\end{algorithm}

\subsection{Power Efficient Configuration Retrieval}\label{subsec:power_efficient_configuration_retrieval}
Once the policy has converged to a configuration able to satisfy the goal of each application, a power efficient configuration can now be retrieved. It is important to notice that, if the policy has converged, but there is one or more applications whose goals are not satisfied (the configuration the policy has converged to is $<4, 1900, 4, 1300>$), it means that, with that goal or application set, those applications cannot be satisfied at all. In this case, we may adopt different approaches; for instance, we could decide to drop applications, starting from the furthest one from its goal, or we could continue the execution until at least one application goal is satisfied.\\
In order to find the most power efficient configuration, we scan all the configurations following the chosen one and select the one with the lowest power consumption. However, we have to be aware of the fact that a following configuration may not imply high throughput, according to the results we showed in \Cref{chap:design}. Therefore, after the configuration has been chosen, we actuated it and verify whether the goals are still satisfied or not. In the latter case, the policy excludes such configuration and looks for another. In the very worst case, the policy converges to the configuration it chose before entering in this step.

\subsection{Configuration Actuation}\label{subsec:configuration_actuation}

The configuration found contains the tuple $<N_B, f_B, N_L, f_L>$ that can be used to enforce the configuration.
Starting from this configuration the policies uses the $N_B$ and $N_L$ parameters to enforce the affinity mask for all the running applications and then $f_B$ and $f_L$ to control \gls{dvfs} actuators.
Both the actions can be performed on \gls{save} Virtual Platform by proper wrapper interfaces that drives the hardware component in the simulator, while they can be implemented in a real system by either \texttt{taskset} utility or \texttt{sched\_setaffinity} for the affinity mask selection, and \texttt{cpufreq-set} for enforcing \gls{dvfs} decisions. Finally, once the new configuration has been actuated, the policy generates the new mapping ratios and generates the number of threads to be mapped on big and LITTLE cores. On the \gls{save} Virtual Platform, we just set the total number of threads to be created (a load-balancing algorithm takes care of properly spread the workload among the cores), while, on the Odroid XU3, we transfer information about thread mapping to the applications, which allocate threads to the cores using \texttt{sched\_setaffinity} command.

\subsection{Configuration Reset}\label{subsec:configuration_reset}

When the application set changes, the system usage changes and so we need to reset some values before retrieving a new configuration. For this reason, we reset the indexes $c_{Low}$ and $c_{High}$, we restore the speedup values in the configuration table to their default values, and we recompute the reference mapping ratio $\bar{p}_{ratio}$. Finally, the policy restarts from \texttt{C612} and repeats its execution as usual.