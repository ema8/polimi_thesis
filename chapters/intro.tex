\chapter{Introduction} \label{chap:intro}

\epigraph{
\itshape
E debbasi considerare come non \`e cosa pi\`u difficile a trattare, n\'e pi\`u dubia a riuscire, n\'e pi\`u pericolosa a maneggiare, che farsi capo ad introdurre nuovi ordini.
}{
\itshape
Niccol\'o Machiavelli, Il Principe
}$\:$\\
This chapter provides an introduction to our thesis. In particular, we describe the main component of the context of this work, and then we briefly present our proposal.\\
In \Cref{sec:context_definition} we describe the context of this work, while \Cref{sec:asymmetric_multiprocessing} introduces the architecture we are going to target. \Cref{sec:thesis_goal} gives an overview of our proposed solution, and, finally, \Cref{sec:thesis_organization} outlines the structure of this thesis.

\section{Context Definition} \label{sec:context_definition}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{figures/introduction/transistor_count}
\caption{Transistor count in Intel microprocessors}
(logarithmic scale)
\label{fig:transistor_count}
\end{figure}

Thanks to the advancements in technology and methodologies, we are now living in an age where computing performance plays a key role in many fields, ranging from finance to cutting-edge research. 
For decades performance improvements have been achieved by increasing the operating frequency of the used processing units, and users benefited from it without impact on the complexity of their programs.
Such enhancements in computing systems were enabled by improvements in transistor technologies according to the Moore's law \cite{moore} and the Dennard's scaling law \cite{dennard1974design}; indeed, every year reduction of MOS transistors size implied more transistors to fit in the same area of the integrated circuit, whereas the power density remained roughly constant.
\Cref{fig:transistor_count} shows the transistor count trend in Intel microprocessors from 1971 to 2015 \cite{intel_processors}; we can notice that, in almost 45 years, the number of transistors moved from 2300 (Intel 4004 processor \cite{intel_4004}, 1971) to 5.56 billion (18-core Xeon Haswell-E5 \cite{intel_xeon_haswell_e5}, 2014). 
However, this trend is not true anymore; indeed, the density of the transistors, in conjunction with higher frequencies, makes it unfeasible to dissipate the high thermal power generated from such small surfaces \cite{borkar2011future, Miyoshi:2002:CPS:514191.514200}. Hence, academy and companies started to look for new approaches to deal with power wall limit.
As a result, to cope with the performance demand, companies adopted multi-cores and parallel solutions \cite{pham2005design, Parkhurst:2006:SCM:1233501.1233516, 10.1109/MC.2008.209, 1430623}.
This choice shifted the burden of performance improvement to programmers, who have to dive into complexity to achieve the performance they need.\\
Nowadays, Multi-cores systems are available in different forms according to costumers' needs. Indeed, it is easy to find quad-core processors on consumer electronics (like Intel i5 \cite{intel_i5} and i7 \cite{intel_i7} series or AMD A-Series \cite{amd_a_series}) and 16-cores on enterprise class server nodes (for instance, Intel Xeon family \cite{intel_xeon}, IBM Power Systems \cite{ibm_power_systems}, and Oracle SPARC Systems \cite{oracle_sparc_systems}), and, given the number of devices spread around the world, it is straightforward to estimate the impact on the global energy consumption scale. 
To mitigate this problem, various techniques have been developed, such as clock gating, \gls{dvfs} and power state switching. 
More recently, a promising approach to pursue performance while keeping energy consumption under control is the exploitation of a \gls{hsa} \cite{hsa-fundation}.
An \gls{hsa} combines different kinds of processing units offering different performance/power consumption trade-offs, like \glspl{cpu}, \glspl{gpu} and \glspl{fpga}, in a single system, to enable the execution of the applications on the most appropriate/convenient kind of resource. 
\glspl{cpu} can efficiently run generic tasks, \glspl{gpu} are optimal for massively parallel repetitive tasks, and \glspl{fpga} can be (dynamically) configured to provide a hardware implementation of a software description for an efficient execution. 
Hence, a system aware of its underlying architecture and the tasks characteristics and needs can exploit the predominant feature of its specific resources.
Nowadays available techniques can also be leveraged to improve the efficiency of every single used computational resource and by doing so the overall system benefits of an even better energy efficiency.\\
On the other hand, applications workloads have been changing through the years \cite{hpc1, hpc2, hpc3}; indeed, while workload used to be static and high-performance oriented, the actual scenario is typically composed of various and flexible on-demand computing workloads. In addiction, such new workloads have different requirements as well, in terms of throughput, power and energy consumption and so on. This situation requires a different approach to face actual workloads. Again, \glspl{hsa} are to the most convenient solution to tackle such problem; indeed, thanks to their nature, \glspl{hsa} may be used to satisfy different workload goals, in terms of both performance and power/energy consumption. For these reasons, \glspl{hsa} currently in use also in \gls{hpc} systems; indeed, the top supercomputer in July 2015, according TOP500 list \cite{top_500}, is \textbf{Tianhe-2}, a system that features both Intel Xeon E5-2692 and Intel Xeon Phi 31S1P coprocessor \cite{intel_xeon_phi_31s1p}, while other supercomputers, like \textbf{Titan} and \textbf{Piz Daint}, are accelerated by NVIDIA \glspl{gpu}. On the other hand, \glspl{hsa} appear also in July 2015 Green500 list \cite{green_500}, the ranking of the most energy-efficient supercomputers in the world. \glspl{hsa} dominate the top places of the Green 500 list; indeed, the most energy-efficient supercomputer is \textbf{Shoubu}, a heterogeneous system composed of Intel Haswell \glspl{cpu} \cite{haswell} and PEZY-SC \cite{pezy_sc} many-core accelerators, which also feature second and third ranked supercomputers in this list.

\section{Asymmetric Multiprocessing} \label{sec:asymmetric_multiprocessing}
An interesting kind of \gls{hsa} are the \textit{asymmetric multiprocessor systems}, i.e. systems composed of processors with different features. The ARM \textbf{big.LITTLE} technology \cite{bigLITTLE} is an example of asymmetric multiprocessor. This solution is mainly employed in embedded systems and mobile devices, thanks to its power and energy efficient nature. Indeed, many mobile devices are powered by ARM big.LITTLE technology; for instance, Samsung implemented such solution inside their Exynos \gls{soc} for Samsung Galaxy S4 \cite{samsung_s4}, S5 \cite{samsung_s5}, and S6 \cite{samsung_s6} device, as well as Qualcomm for Snapdragon \gls{soc} (LG G4 \cite{lg_g4}, HTC One M9 \cite{htc_one_m9}).\\
ARM big.LITTLE technology is composed of two clusters of processors: a \textit{big} cluster and a \textit{LITTLE} cluster. Although both clusters share the same \gls{isa}, their pipelines are different in terms of length and complexity, which means different power and performance levels. As result, big cluster is able of providing high performance, in exchange for higher power consumption, whereas LITTLE cluster is designed to be power efficient, delivering lower performance.
Therefore, ARM big.LITTLE design allows it to satisfy two fundamental requirements: high compute capability and very low power consumption. In our vision, we want to tackle the problem of managing and optimizing the power consumption of the ARM big.LITTLE architecture.

\section{Thesis Goal} \label{sec:thesis_goal}
In a system where one or more applications are running, it is crucial to guarantee the \gls{qos} of each application, in particular when their workloads are various and dynamic rather than static. On the ARM big.LITTLE, as well as on other \glspl{hsa}, there are no procedures that control and ensure such requirements. For this reason, our thesis aims at introducing a software policy for ARM big.LITTLE architecture, in order to both monitor and satisfy \gls{qos} requirements, and, at the same time, optimize the system power consumption, exploiting ARM big.LITTLE features.\\
To this end, we propose a workload-aware run-time resource management policy that exploits \gls{dvfs} (combined with dynamic core assignment) and task allocation (at thread level) that:
\begin{itemize}
\item manages power consumption;
\item guarantees performance constraints of the running applications;
\item is able to adapt itself to varying system load by exploiting online performance measurements;
\item balances the workload among the available heterogeneous cores.
\end{itemize}
At first, the proposed policy has been developed in high-level simulator for fast resource management policy prototyping. Such simulator, developed in the context of EU \gls{save} Project \cite{save}, is available online at \cite{SaveVP}. Once the policy development and testing on \gls{save} Virtual Platform was completed, it has been implemented and evaluated on a real development board, the Odroid XU3 \cite{odroid}, which is powered by the ARM big.LITTLE architecture. Finally, the policy results were compared with the ones provided by \gls{hmp}, the state of the art heterogeneous scheduler available on the Odroid XU3 development board.

\section{Thesis Organization} \label{sec:thesis_organization}

The work presented in our thesis is organized as follows:
\begin{itemize}
\item \Cref{chap:bckg} explains the necessary background knowledge to understand this work; in particular it introduces the \glspl{hsa}, focusing on the ARM big.LITTLE, our target architecture, and EU \gls{save} Project;
\item \Cref{chap:soa} presents an overview of the state of the art, showing different solutions oriented to both reduce power/energy consumption and guarantee \gls{qos} in \glspl{hsa}; then, it provides an analysis of \gls{hmp}, the heterogeneous scheduler the ARM big.LITTLE platform features;
\item \Cref{chap:design} defines the problem this work wants to tackle and explains the solution we propose, i.e. a workload-aware run-time resource management policy for the ARM big.LITTLE architecture; 
\item \Cref{chap:implementation} explains the details of the proposed policy by exposing, step by step, its features and structure;
\item \Cref{chap:results} evaluates the results of our policy on both \gls{save} Virtual Platform and on the Odroid XU3 development board, and compare such results with \gls{hmp};
\item \Cref{chap:conclusion} presents a general overview of the results of this thesis, analyzes the limitations of our work, and describes possible future works.
\end{itemize}
