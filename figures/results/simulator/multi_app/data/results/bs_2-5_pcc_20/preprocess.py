import os
import sys

def getFiles(mypath):
	f = []
	for (dirpath, dirnames, filenames) in os.walk(mypath):
	    f.extend(filenames)
	    break

	return f

def formatApp(app):
	if app == "bin":
		return "Binning"
	if app == "bs":
		return "Black Scholes"
	if app == "pear":
		return "Pearson"

def sumPower(powerFileName):
    f = open(powerFileName)
    fout = open("power.txt", "w")
    lines = f.readlines()
    i = 1
    power = 0
    for l in lines:
        data = l.strip().split("\t")
        power += float(data[2])
        if i % 8 == 0:
            fout.write(data[3] + "\t" + power.__str__() + "\n")
            power = 0
        i+=1


path = "./"

os.system("rm performance.dat")
os.system("rm power.dat")

files = getFiles(path)

reportPerformance = []
reportPower = []

for f in files:
	if ".html" in f:
		lines = open(path + "/" + f).readlines()
		for l in lines:
			data = l.strip().split("\t")
			fileInfo = f[0:-4]

			if("PERFORMANCE" in l):
				finalInfo = [fileInfo, data[1], data[2], data[3], data[4], data[5] ]
				reportPerformance.append(finalInfo)

			if("INSTANT_POWER" in l):
				finalInfo = [fileInfo, data[1], data[2], data[3]]
				reportPower.append(finalInfo)

out = open("performance.dat", "w")
for r in reportPerformance:
	out.write("\t".join(r) + "\n")
out.close()

out = open("power.dat", "w")
for r in reportPower:
	out.write("\t".join(r) + "\n")
out.close()

sumPower("power.dat")